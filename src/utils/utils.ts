export const range = (start:number, stop:number) => Array.from({ length: (stop - start) + 1}, (_, i) => start + i);

export const uniq = (array:Array<any>) => {
    const knownElements = new Set();
    for (const elem of array) {
        knownElements.add(elem); // 同じ値を何度追加しても問題ない
    }
    return Array.from(knownElements);
}