import * as React from 'react';
import {NavigationComponent} from "../../components/navigation_component";
import {GlobalStates} from "../../stores/states/states";
import {Dispatch} from "redux";
import * as Actions from "./actions";
import {EntryConfirmPageState} from "../../stores/states/entry_confirm_page/state";
import {connect} from "react-redux";
import {RouteComponentProps} from "react-router";
export interface EntryConfirmPageProps {
    name: string;
    email: string;
    age: string;
    desiredOccupation: string;
    applyForReason: string;
}

interface EntryConfirmPageHandler {
    onPageStart(entryID:string): void;

    confirmInfo(name:string,email:string,age:string,desiredOccupation:string,reasonForApply:string): void;
}

type Props = EntryConfirmPageProps & EntryConfirmPageHandler & History & RouteComponentProps<{ entry_id: string }>;

class Component extends React.Component<Props, EntryConfirmPageState> {
    componentDidMount(): void {
        this.props.onPageStart(this.props.match.params.entry_id);
    }

    public render(): JSX.Element {
        return <div id="entry-confirm" className="container-fluid">
            <NavigationComponent/>

            <div className="card">
                <div className="card-body">

                    <div className={"row justify-content-center"}>
                        <div className={"align-items-center"}>
                            <h1>エントリー</h1>
                        </div>
                    </div>


                    <div className={"row item justify-content-center"}>
                        <div className={"row col-12 col-md-5 align-items-center"}>
                            <div className="col-4 col-form-label">
                                氏名
                            </div>
                            <div className="col-8 row">
                                <span>{this.props.name}</span>
                            </div>
                        </div>
                    </div>

                    <div className={"row item justify-content-center"}>
                        <div className={"row col-12 col-md-5 align-items-center"}>
                            <div className="col-4 col-form-label">
                                Email
                            </div>
                            <div className={"col-8 row"}>
                                <span>{this.props.email}</span>
                            </div>
                        </div>
                    </div>

                    <div className={"row item justify-content-center"}>
                        <div className={"row col-12 col-md-5 align-items-center"}>
                            <label className="col-4 col-form-label">
                                年齢※
                            </label>
                            <div className={"col-8 row"}>
                                <span>{this.props.age}</span>
                            </div>
                        </div>
                    </div>

                    <div className={"row item justify-content-center"}>
                        <div className={"row col-12 col-md-5 align-items-center"}>
                            <label className="col-4 col-form-label">
                                希望職種※
                            </label>
                            <div className="col-8 row">
                                <span>{this.props.desiredOccupation}</span>
                            </div>
                        </div>
                    </div>


                    <div className={"row item justify-content-center"}>
                        <div className={"row col-12 col-md-5 align-items-center"}>
                            <label className="col-4 col-form-label">
                                希望理由※
                            </label>
                            <div className="col-8 row">
                                <p>{this.props.applyForReason}</p>
                            </div>
                        </div>
                    </div>

                    <div className={"row item justify-content-center"}>
                        <div className={"row col-12 col-md-5 align-items-center"}>
                            <label className="col-4 col-form-label">
                            </label>
                            <div className={"col-8 row"}>
                                <button type="button" onClick={_ => this.props.confirmInfo(this.props.name,this.props.email,this.props.age,this.props.desiredOccupation,this.props.applyForReason)}
                                        className="col-12 btn btn-info center-block">応募
                                </button>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
        </div>

    }
}

const mapStateToProps = (globalStates: GlobalStates): EntryConfirmPageProps => {
    const entryConfirmPageState = globalStates.pageStates.entryConfirmPage;
    const entryID = entryConfirmPageState.applyEntryID;

    const defaultProps:EntryConfirmPageProps = {
        name: '',
        email: '',
        age: '',
        desiredOccupation: '',
        applyForReason: '',
    }

    if (globalStates.states.applyEntryEntity === null) {
        return defaultProps;
    }

    if (globalStates.states.applyEntryEntity.id !== entryID) {
        return defaultProps;
    }

    const entryEntity = globalStates.states.applyEntryEntity;
    return {
        name: entryEntity.name,
        email: entryEntity.email,
        age: entryEntity.age.toString(),
        desiredOccupation: entryEntity.desiredOccupation,
        applyForReason: entryEntity.reasonForApply,
    }
}

const mapDispatchToProps = (dispatch: Dispatch): EntryConfirmPageHandler => {
    return {
        onPageStart: (entryID:string) => dispatch(Actions.onPageStart(entryID)),
        confirmInfo: (name:string,email:string,age:string,desiredOccupation:string,reasonForApply:string) => dispatch(Actions.confirmInfo(name,email,age,desiredOccupation,reasonForApply))
    }
}


export const EntryConfirmPage = connect(mapStateToProps, mapDispatchToProps)(Component)