import {Dispatch} from "redux";
import {getState} from "../../stores/get_state";
import {GlobalService} from "../../service/global_service";
import {URLS} from "../../configs";
import {EntryConfirmPageActions} from "../../stores/actions/actions";
import {EntryCloudFunctionPostData} from "../../service/firebase_service";


export const onPageStart = (entryID:string):any => async (dispatch: Dispatch, getState:getState,service:GlobalService) => {
    dispatch(EntryConfirmPageActions.setEntryApplyID(entryID))
}


export const confirmInfo = (name:string,email:string,age:string,desiredOccupation:string,reasonForApply:string):any => async (dispatch: Dispatch, getState:getState,service:GlobalService) => {
    const firebaseService = service.firebaseService;
    const pageState = getState().pageStates.entryConfirmPage;


    const id = await firebaseService.postNewEntry({
        age: parseInt(age),
        desired_occupation: desiredOccupation,
        email: email,
        name: name,
        reason_for_apply: reasonForApply,
    })

    const sendGridEntryPostData: EntryCloudFunctionPostData = {
        name: name,
        email:email,
        age: parseInt(age),
        desired_occupation: desiredOccupation,
        apply_for_reason: reasonForApply,
    }

    try {
        service.firebaseService.firebaseClient.functions("us-central1").httpsCallable("sendGridOnCall")(
            sendGridEntryPostData
        );
    }catch (e) {
        console.log(e);
    }

    service.history.push(URLS.URL_ENTRY_COMPLETE);
}