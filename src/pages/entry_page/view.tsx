import * as React from 'react';
import {NavigationComponent} from "../../components/navigation_component";
import {Dispatch} from "redux";
import {connect} from "react-redux";
import * as Handlers from "./handlers";
import {GlobalStates} from "../../stores/states/states";
import {EntryPageState} from "../../stores/states/entry_page/state";
import {ageRange} from "../../constants/constants";
import {FooterComponent} from "../../components/footer_component";
import {RecruitProp} from "../common_props";
import {mapRecruitsStatesToProps} from "../common_mapper";

export interface EntryPageProps {
    userName: string;
    email: string;
    age: number;
    desiredOccupation: string;
    reasonForApply: string;
    recruits: RecruitProp[];
    formError: string | null;
}

export interface EntryPageHandler {
    handleOnNameInputValueChanges(value: string): void

    handleEmailInputValueChanges(value: string): void

    handleAgeInputValueChanges(value: string): void

    handleDesiredOccupationChanges(value: string): void

    handleReasonForApply(value: string): void

    handleConfirm(): void

    onPageStart(): void
}

type Props = EntryPageProps & EntryPageHandler & History;


class PageComponent extends React.Component<Props, EntryPageState> {

    componentDidMount(): void {
        this.props.onPageStart();
    }

    public render(): JSX.Element {
        return <div id="entry" className="container-fluid">
            <NavigationComponent/>

            <div className="card">
                <div className="card-body">

                    <div className={"row justify-content-center"}>
                        <div className={"align-items-center"}>
                            <h1>エントリー</h1>
                        </div>
                    </div>

                    <div className={"row item justify-content-center"}>
                        <div className={"row col-12 col-md-5 align-items-center"}>
                            <label className="col-4 col-form-label">
                                氏名
                            </label>
                            <div className="col-8 row">
                                <input className={"col-12"} type="text"
                                       onChange={e => this.props.handleOnNameInputValueChanges(e.target.value)}
                                       value={this.props.userName}/>
                            </div>
                        </div>
                    </div>

                    <div className={"row item justify-content-center"}>
                        <div className={"row col-12 col-md-5 align-items-center"}>
                            <label className="col-4 col-form-label">
                                Email
                            </label>
                            <div className={"col-8 row"}>
                                <input className={"col-12"} type="mail"
                                       onChange={e => this.props.handleEmailInputValueChanges(e.target.value)}
                                       value={this.props.email}/>
                            </div>
                        </div>
                    </div>

                    <div className={"row item justify-content-center"}>
                        <div className={"row col-12 col-md-5 align-items-center"}>
                            <label className="col-4 col-form-label">
                                年齢※
                            </label>
                            <div className={"col-8 row"}>
                                <select onChange={e => this.props.handleAgeInputValueChanges(e.target.value)}
                                        value={this.props.age} className="col-12 form-select"
                                        aria-label="Default select example">
                                    {ageRange.map(e => <option key={e} value={e}>{e}</option>)}
                                </select>
                            </div>
                        </div>
                    </div>

                    <div className={"row item justify-content-center"}>
                        <div className={"row col-12 col-md-5 align-items-center"}>
                            <label className="col-4 col-form-label">
                                希望職種※
                            </label>
                            <div className="col-8 row">
                                <select className="col-12 form-select"
                                        onChange={e => this.props.handleDesiredOccupationChanges(e.target.value)}
                                        aria-label="Default select example">
                                    {this.props.recruits.map((e, k) => <option value={e.position} key={k}>{e.position}</option>)}
                                </select>
                            </div>
                        </div>
                    </div>


                    <div className={"row item justify-content-center"}>
                        <div className={"row col-12 col-md-5 align-items-center"}>
                            <label className="col-4 col-form-label">
                                希望理由※
                            </label>
                            <div className="col-8 row">
                                <textarea className="form-control" rows={5}
                                          onChange={e => this.props.handleReasonForApply(e.target.value)}
                                          value={this.props.reasonForApply}></textarea>
                            </div>
                        </div>
                    </div>

                    <div className={"row item justify-content-center"}>
                        <div className={"row col-12 col-md-5 align-items-center"}>
                            <label className="col-4 col-form-label">
                            </label>
                            <div className={"col-8 row"}>
                                <button type="button" className="col-12 btn btn-info center-block"
                                        onClick={_ => this.props.handleConfirm()}>申込み
                                </button>
                            </div>
                        </div>
                    </div>

                    <div className={"row item justify-content-center"}>
                        <div className={"row col-12 col-md-5 align-items-center"}>
                            <label className="col-4 col-form-label">
                            </label>
                            <div className={"col-8 row"}>
                                <p>※ 必須項目</p>
                            </div>
                        </div>
                    </div>


                    {this.props.formError !== null && (
                        <div className={"row item justify-content-center"}>
                            <div className="col-5 alert alert-warning" role="alert">
                                {this.props.formError}
                            </div>
                        </div>
                    )}

                </div>
            </div>
            <FooterComponent/>
        </div>
    }
}


const mapStateToProps = (globalStates: GlobalStates): EntryPageProps => {
    const pageState = globalStates.pageStates.entryPageState;
    const recruitsEntities = globalStates.states.recruits;
    return {
        userName: pageState.name,
        email: pageState.email,
        age: pageState.age,
        desiredOccupation: pageState.desiredOccupation,
        reasonForApply: pageState.reasonForApply,
        recruits: mapRecruitsStatesToProps(recruitsEntities),
        formError: pageState.formError,
    }
}

const mapDispatchToProps = (dispatch: Dispatch) => {
    return {
        onPageStart: () => dispatch(Handlers.onPageStart()),
        handleEmailInputValueChanges: Handlers.setEmail(dispatch),
        handleOnNameInputValueChanges: Handlers.setName(dispatch),
        handleAgeInputValueChanges: Handlers.setAge(dispatch),
        handleDesiredOccupationChanges: Handlers.setDesiredOccupation(dispatch),
        handleReasonForApply: Handlers.setReasonForApply(dispatch),
        handleConfirm: () => dispatch(Handlers.sendInfo())
    }
}


export const EntryPage = connect(mapStateToProps, mapDispatchToProps)(PageComponent)