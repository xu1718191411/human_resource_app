import {Dispatch} from "redux";
import {EntryPageActions} from '../../stores/actions/actions';
import {getState} from "../../stores/get_state";
import {GlobalService} from "../../service/global_service";
import {EntitiesActions} from "../../stores/actions";
import {EntryEntity} from "../../stores/states/common_states";
import {URLS} from "../../configs";
import {parseRecruits} from "../../parsers/parsers";

export const onPageStart = ():any => {
    return async (dispatch: Dispatch,getState: getState, service: GlobalService) => {
        console.log("on page started");
        clearForm(dispatch);
        const recruits = await service.firebaseService.getRecruits();
        const res = parseRecruits(recruits);
        dispatch(EntitiesActions.setRecruits(res));
    }
}

export const setEmail = (dispatch: Dispatch) => {
    return (email: string) => {
        dispatch(EntryPageActions.setEmail(email))
    }
}

export const setName = (dispatch: Dispatch) => {
    return (name: string) => {
        dispatch(EntryPageActions.setName(name));
    }
}

export const setAge = (dispatch: Dispatch) => {
    return (age: string) => {
        dispatch(EntryPageActions.setAge(parseInt(age)));
    }
}

export const setDesiredOccupation = (dispatch: Dispatch) => {
    return (desiredOccupation: string) => {
        dispatch(EntryPageActions.setDesiredOccupation(desiredOccupation))
    }
}

export const setReasonForApply = (dispatch: Dispatch) => {
    return (reasonForApply: string) => {
        dispatch(EntryPageActions.setReasonForApply(reasonForApply))
    }
}


export const sendInfo = (): any => async (dispatch: Dispatch, getState: getState, service: GlobalService) => {
    const pageState = getState().pageStates.entryPageState;

    const formError = verifyForm(pageState.name,pageState.email,pageState.reasonForApply);
    if (formError !== null) {
        dispatch(EntryPageActions.setFormError(formError));
        return;
    }
    const randomID = Math.random().toString(32).substring(2)

    const applyEntryEntity: EntryEntity = {
        id: randomID,
        name: pageState.name,
        email: pageState.email,
        age: pageState.age,
        desiredOccupation: pageState.desiredOccupation,
        reasonForApply: pageState.reasonForApply,
    }

    dispatch(EntitiesActions.setApplyEntryEntity(applyEntryEntity));
    service.history.push(URLS.GET_URL_ENTRY_CONFIRM(randomID));
}

const clearForm = (dispatch: Dispatch) => {
    dispatch(EntryPageActions.setEmail(''));
    dispatch(EntryPageActions.setName(''));
    dispatch(EntryPageActions.setAge(0));
    dispatch(EntryPageActions.setDesiredOccupation(''));
    dispatch(EntryPageActions.setReasonForApply(''))
}

const verifyForm = (name:string,email:string,applyforReason:string):string | null => {

    if (name === "" || name === undefined) {
        return "氏名を入力してください";
    }

    if (email === "" || email === undefined) {
        return "メールを入力してください";
    }

    if (!validateEmail(email)) {
        return "ただしいメールアドレスを入力してください";
    }

    if (applyforReason === "" || applyforReason === undefined) {
        return "希望理由を入力してください";
    }


    return null;
}

const validateEmail = (email:string):boolean => {
    const re = /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return re.test(String(email).toLowerCase());
}