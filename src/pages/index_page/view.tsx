import * as React from 'react'
import {Container,Row,Col} from "react-bootstrap";
export interface IndexPageProps {
    title:string;
}

export class IndexPage extends React.Component<IndexPageProps> {
    public render(): JSX.Element {
        return <Container>
            <Row>
                <Col>1 of 1</Col>
            </Row>
        </Container>;
    }
}