import * as React from 'react';
import {AdminSideNavigation} from "../../../components/admin/admin_side_navigation";
import {RecruitProp} from "../../common_props";
import {GlobalStates} from "../../../stores/states/states";
import {Dispatch} from "redux";
import * as Handlers from "./handlers";
import {connect} from "react-redux";
import {mapRecruitsStatesToProps} from "../../common_mapper";
import {GET_URL_ADMIN_RECRUIT_DETAIL} from "../../../configs/urls";
import {Link} from "react-router-dom";


export interface AdminRecruitsPageProps{
    recruits:RecruitProp[],
}

interface AdminRecruitsPageHandlers {
    onPageStart():void
}

type Props = AdminRecruitsPageProps & History & AdminRecruitsPageHandlers;

export class PageComponent extends React.Component<Props>{
    componentDidMount(): void {
        this.props.onPageStart();
    }

    public render(): JSX.Element {
        return <div id="admin-entries-edit" className="container-fluid">
            <div id={"admin-entries-body"} className={"row"}>

                <div className={"col-md-2 side-menu m-2 mb-12 card"}>
                    <AdminSideNavigation/>
                </div>

                <div className={"col-md m-2"}>

                    <div className={"row card mt-2 admin-title text-center"}>
                        <div className={"card-head"}>
                            <h3>応募職種一覧</h3>
                        </div>
                    </div>

                    <div className={"row card mt-2 admin-body"}>
                        <div className={"card-body"}>


                            <table className="table table-bordered align-middle mt-3">
                                <thead>
                                <tr>
                                    <th scope="col">職種</th>
                                    <th scope="col">更新日</th>
                                    <th scope="col">追加日</th>
                                    <th scope="col">操作</th>
                                </tr>
                                </thead>
                                <tbody>

                                {this.props.recruits.map((e,k)=>
                                    <tr key={k}>
                                    <th scope="row">{e.position}</th>
                                    <td>2020-12-20</td>
                                    <td>2020-11-12</td>
                                    <td>
                                        <button className={"btn btn-info"}>
                                            <Link to={GET_URL_ADMIN_RECRUIT_DETAIL(e.id)}>詳細</Link>
                                        </button>
                                    </td>
                                </tr>
                                )}


                                </tbody>
                            </table>

                        </div>
                    </div>

                </div>

            </div>
        </div>;
    }
}

const mapStateToProps = (globalStates:GlobalStates):AdminRecruitsPageProps => {
    return {
        recruits:mapRecruitsStatesToProps(globalStates.states.recruits),
    }
}

const mapDispatchToProps = (dispatch:Dispatch):AdminRecruitsPageHandlers => {
    return {
        onPageStart: () => dispatch(Handlers.onPageStart())
    }
}


export const AdminRecruitsPage = connect(mapStateToProps,mapDispatchToProps)(PageComponent)
