import {getState} from "../../../stores/get_state";
import {GlobalService} from "../../../service/global_service";
import {Dispatch} from "redux";
import {parseRecruits} from "../../../parsers/parsers";
import {EntitiesActions} from "../../../stores/actions";
import {URLS} from "../../../configs";

export const onPageStart = ():any => {
    return async (dispatch:Dispatch, getStates:getState, service:GlobalService) => {
        const isLogin = await service.firebaseService.firebaseAuthClient.checkISLogin();
        if (!isLogin) {
            service.history.push(URLS.URL_ADMIN_LOGIN);
            return;
        }
        console.log("on page start");
        const res = await service.firebaseService.getRecruits()
        const recruits = parseRecruits(res);
        dispatch(EntitiesActions.setRecruits(recruits));
    }
}