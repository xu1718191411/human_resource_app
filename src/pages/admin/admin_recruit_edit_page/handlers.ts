import { getState } from "../../../stores/get_state";
import { GlobalService } from "../../../service/global_service";
import { Dispatch } from "redux";
import { AdminRecruitEditPageActions } from "../../../stores/actions/actions";
import { parseRecruit } from "../../../parsers/parsers";
import { URL_ADMIN_RECRUITS } from "../../../configs/urls";
import {URLS} from "../../../configs";

export const handleOnPageStart = (recruitID: string | undefined): any => {
    return async (dispatch: Dispatch, getStates: getState, service: GlobalService) => {
        const isLogin = await service.firebaseService.firebaseAuthClient.checkISLogin();
        if (!isLogin) {
            service.history.push(URLS.URL_ADMIN_LOGIN);
            return;
        }
        dispatch(AdminRecruitEditPageActions.setRecruitID(recruitID));
        if (recruitID === undefined) {
            clearForm(dispatch);
            return;
        }
        const result = await service.firebaseService.getRecruit(recruitID)
        const recruitEntity = parseRecruit(result.data(),result.id)
        dispatch(AdminRecruitEditPageActions.setPosition(recruitEntity.position));
        dispatch(AdminRecruitEditPageActions.setContent(recruitEntity.content));
        dispatch(AdminRecruitEditPageActions.setQualification(recruitEntity.qualification));
        dispatch(AdminRecruitEditPageActions.setEmploymentStatus(recruitEntity.employment_status));
        dispatch(AdminRecruitEditPageActions.setEmploymentStatus(recruitEntity.employment_status));
    }
}

export const handleOnPositionInputChanges = (value: string): any => {
    return async (dispatch: Dispatch, getStates: getState, service: GlobalService) => {
        console.log("handleOnPositionInputChanges");
        dispatch(AdminRecruitEditPageActions.setPosition(value));
    }
}

export const handleOnContentInputChanges = (value: string): any => {
    return async (dispatch: Dispatch, getStates: getState, service: GlobalService) => {
        console.log("handleOnContentInputChanges");
        dispatch(AdminRecruitEditPageActions.setContent(value));
    }
}

export const handleOnQualificationInputChanges = (value: string): any => {
    return async (dispatch: Dispatch, getStates: getState, service: GlobalService) => {
        console.log("handleOnQualificationInputChanges");
        dispatch(AdminRecruitEditPageActions.setQualification(value));
    }
}


export const handleOnEmploymentStatusInputChanges = (value: string): any => {
    return async (dispatch: Dispatch, getStates: getState, service: GlobalService) => {
        console.log("handleOnEmploymentStatusInputChanges");
        dispatch(AdminRecruitEditPageActions.setEmploymentStatus(value));
    }
}

export const handleOnCreateNewRecruit = (): any => {
    return async (dispatch: Dispatch, getStates: getState, service: GlobalService) => {
        const pageState = getStates().pageStates.adminRecruitEditPageStates;

        if (pageState.position.length === 0) {
            return;
        }

        if (pageState.content.length === 0) {
            return;
        }

        if (pageState.qualification.length == 0) {
            return;
        }

        if (pageState.employmentStatus.length === 0) {
            return;
        }


        const res = await service.firebaseService.postRecruit({
            position: pageState.position,
            content: pageState.content,
            qualification: pageState.qualification,
            employment_status: pageState.employmentStatus,
        })
        service.history.push(URL_ADMIN_RECRUITS);
    }
}

export const handleOnUpdateRecruit = ():any => {
    return async (dispatch: Dispatch, getStates: getState, service: GlobalService) => {
        const pageState = getStates().pageStates.adminRecruitEditPageStates;
        if (pageState.position.length === 0) {
            return;
        }

        if (pageState.content.length === 0) {
            return;
        }

        if (pageState.qualification.length == 0) {
            return;
        }

        if (pageState.employmentStatus.length === 0) {
            return;
        }

        if (pageState.recruitID === undefined) {
            return;
        }

        await service.firebaseService.updateRecruit(pageState.recruitID,pageState.position,pageState.content,pageState.qualification,pageState.employmentStatus);

        service.history.push(URL_ADMIN_RECRUITS);
    }
}


export const handleOnDeleteRecruit = (recruitID:string | undefined):any => {
    return async (dispatch: Dispatch, getStates: getState, service: GlobalService) => {
        if (recruitID === undefined) {
            dispatch(AdminRecruitEditPageActions.setDeletingID(null));
            return;
        }
        dispatch(AdminRecruitEditPageActions.setDeletingID(recruitID));
    }
}


export const handleCancelDelete = ():any => {
    return async (dispatch: Dispatch, getStates: getState, service: GlobalService) => {
        dispatch(AdminRecruitEditPageActions.setDeletingID(null));
    }
}

export const handleDoDelete = (recruitID:string | null):any => {
    return async (dispatch: Dispatch, getStates: getState, service: GlobalService) => {
        if (recruitID === null) {
            return;
        }
        await service.firebaseService.deleteRecruit(recruitID)
        service.history.push(URL_ADMIN_RECRUITS);
    }
}


const clearForm = (dispatch:Dispatch) => {
    dispatch(AdminRecruitEditPageActions.setRecruitID(undefined));
    dispatch(AdminRecruitEditPageActions.setPosition(''));
    dispatch(AdminRecruitEditPageActions.setContent(''));
    dispatch(AdminRecruitEditPageActions.setQualification(''));
    dispatch(AdminRecruitEditPageActions.setEmploymentStatus(''));
    dispatch(AdminRecruitEditPageActions.setEmploymentStatus(''));
}