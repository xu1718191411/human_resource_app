import * as React from 'react';
import { AdminSideNavigation } from "../../../components/admin/admin_side_navigation";
import { RouteComponentProps } from "react-router";
import { GlobalStates } from "../../../stores/states/states";
import { Dispatch } from "redux";
import * as Handlers from "./handlers";
import { connect } from "react-redux";
import {Link} from "react-router-dom";
import { URL_ADMIN_RECRUITS } from "../../../configs/urls";
import Modal from "react-modal";
import { customStyles } from "../../../constants/constants";
import { RecruitProp } from "../../common_props";
import {RecruitEntity} from "../../../stores/states/common_states";
import {mapRecruitStateToProp} from "../../common_mapper";
type PageStatus = 'create' | 'edit'

interface AdminRecruitEditPageProps {
    pageStatus:PageStatus,
    recruitID:string | undefined,
    position: string;
    content: string;
    qualification: string;
    employment_status: string;
    deletingRecruit: RecruitProp | null;
}

interface AdminRecruitEditPageHandlers {
    handleOnPageStart(recruitID:string | undefined):void;

    handleOnPositionInputChanges(value: string): void;

    handleOnContentInputChanges(value: string): void;

    handleOnQualificationInputChanges(value: string): void;

    handleOnEmploymentStatusInputChanges(value: string): void;

    handleOnCreateNewRecruit():void;

    handleOnUpdateRecruit():void;

    handleOnDeleteRecruit(recruitID:string | undefined):void;

    handleCancelDelete():void;

    handleDoDelete(recruitID:string | null):void;
}

type Props =
    AdminRecruitEditPageProps
    & AdminRecruitEditPageHandlers
    & History
    & RouteComponentProps<{ recruit_id: string }>


export class PageComponent extends React.Component<Props> {
    componentDidMount(): void {
        this.props.handleOnPageStart(this.props.match.params.recruit_id);
    }

    public render(): JSX.Element {
        return <div id="admin-recruits-edit" className="container-fluid">

            <div id={"admin-entries-body"} className={"row"}>

                <div className={"col-md-2 side-menu m-2 mb-12 card"}>
                    <AdminSideNavigation/>
                </div>

                <div className={"col-md m-2"}>

                    <div className={"row card mt-2 admin-title text-center"}>
                        <div className={"card-head"}>
                            <div className={"row col-10 justify-content-center"}>
                                {this.props.pageStatus === 'create' && (
                                    <h3 className={"col-5"}>新規職種</h3>
                                )}

                                {this.props.pageStatus === 'edit' && (
                                    <h3 className={"col-5"}>職種編集</h3>
                                )}
                            </div>
                        </div>
                    </div>

                    <div className={"row card mt-2 admin-body"}>
                        <div className={"card-body"}>

                            <div className={"row"}>
                                <button type="button" className="btn btn-default col-1" aria-label="Left Align">
                                    <span className="glyphicon glyphicon-align-left" aria-hidden="true"><Link to={URL_ADMIN_RECRUITS}>←戻る</Link></span>
                                </button>
                            </div>

                            <div className={"row col-10 justify-content-center"}>
                                <div className={"row col-12 col-md-7 align-items-center"}>
                                    <label className="col-4 col-form-label">
                                        ポジション
                                    </label>
                                    <div className="col-8 row">
                                        <input className={"col-12"} type="text" value={this.props.position} onChange={e => this.props.handleOnPositionInputChanges(e.target.value)}/>
                                    </div>
                                </div>
                            </div>

                            <div className={"row col-10 justify-content-center"}>
                                <div className={"row col-12 col-md-7 align-items-center"}>
                                    <label className="col-4 col-form-label">
                                        仕事内容
                                    </label>
                                    <div className="col-8 row">
                                        <textarea className="form-control" rows={3} value={this.props.content} onChange={e => this.props.handleOnContentInputChanges(e.target.value)}></textarea>
                                    </div>
                                </div>
                            </div>

                            <div className={"row col-10 justify-content-center mt-3"}>
                                <div className={"row col-12 col-md-7 align-items-center"}>
                                    <label className="col-4 col-form-label">
                                        応募資格
                                    </label>
                                    <div className="col-8 row">
                                        <textarea className="form-control" rows={3} value={this.props.qualification} onChange={e=>this.props.handleOnQualificationInputChanges(e.target.value)}></textarea>
                                    </div>
                                </div>
                            </div>


                            <div className={"row col-10 justify-content-center mt-3"}>
                                <div className={"row col-12 col-md-7 align-items-center"}>
                                    <label className="col-4 col-form-label">
                                        雇用形態
                                    </label>
                                    <div className="col-8 row">
                                        <textarea className="form-control" rows={3} value={this.props.employment_status} onChange={e => this.props.handleOnEmploymentStatusInputChanges(e.target.value)}></textarea>
                                    </div>
                                </div>
                            </div>


                            <div className={"row col-10 justify-content-center mt-3"}>
                                <div className={"row col-12 col-md-7 align-items-center"}>
                                    <label className="col-4 col-form-label">
                                        ステータス
                                    </label>
                                    <div className="col-8 row">
                                        <select className="col-12 form-select" aria-label="Default select example">
                                            <option>開放</option>
                                            <option>クローズ</option>
                                        </select>
                                    </div>
                                </div>
                            </div>


                            <div className={"row col-12 justify-content-center mt-3"}>
                                {this.props.pageStatus === 'create' && (
                                    <div className={"row col-12 col-md-7 align-items-center"}>
                                        <button onClick={_=>this.props.handleOnCreateNewRecruit()} className={"btn btn-info col-8 mr-3 "}>新規登録</button>
                                    </div>
                                )}

                                {this.props.pageStatus === 'edit' && (
                                    <div className={"row col-12 col-md-7 align-items-center"}>
                                        <button onClick={_=>this.props.handleOnUpdateRecruit()} className={"btn btn-info col-4 mr-3 "}>更新</button>
                                        <button onClick={_=>this.props.handleOnDeleteRecruit(this.props.recruitID)} className={"btn btn-danger col-4"}>削除</button>
                                    </div>
                                )}
                            </div>


                        </div>
                    </div>

                </div>

            </div>

            <Modal isOpen={this.props.deletingRecruit !== null} style={customStyles}>
                <div className="container-fluid">
                    <div className={"row justify-content-center col-12"}>
                        <div className="alert alert-danger col-10 text-center" role="alert">
                            この職種の情報を削除しますか?
                        </div>

                        <div className={"col-10"}>
                            <table className="table table-bordered">
                                <tbody>
                                <tr>
                                    <th scope="row">仕事内容</th>
                                    <td>{this.props.deletingRecruit && this.props.deletingRecruit.position}</td>
                                </tr>
                                <tr>
                                    <th scope="row">応募資格</th>
                                    <td>{this.props.deletingRecruit && this.props.deletingRecruit.qualification}</td>
                                </tr>
                                <tr>
                                    <th scope="row">雇用形態</th>
                                    <td>{this.props.deletingRecruit && this.props.deletingRecruit.employment_status}</td>
                                </tr>
                                </tbody>
                            </table>
                        </div>

                        <div className={"row col-10 col-md-7"}>
                            <div className={"col"}>
                                <button type="button" className="col-10 btn btn-danger" onClick={_=>this.props.handleDoDelete(this.props.deletingRecruit === null ? null: this.props.deletingRecruit.id)}>削除</button>
                            </div>
                            <div className={"col"}>
                                <button type="button" className="col-10 btn btn-primary" onClick={_=>this.props.handleCancelDelete()}>キャンセル</button>
                            </div>
                        </div>
                    </div>
                </div>
            </Modal>
        </div>;
    }
}


const mapStateToProps = (globalStates: GlobalStates): AdminRecruitEditPageProps => {
    const pageState = globalStates.pageStates.adminRecruitEditPageStates;
    return {
        pageStatus: pageState.recruitID === undefined? 'create': 'edit',
        recruitID:pageState.recruitID,
        position: pageState.position,
        content: pageState.content,
        qualification: pageState.qualification,
        employment_status: pageState.employmentStatus,
        deletingRecruit: mapDeletingRecruit(pageState.deletingID,globalStates.states.recruits),
    }
}

const mapDeletingRecruit = (deletingID:string | null, recruits: RecruitEntity[]):RecruitProp | null => {
    if (deletingID === null) {
        return null;
    }

    const index = recruits.findIndex(e => e.id === deletingID);

    if (index === -1) {
        return null;
    }

    return mapRecruitStateToProp(recruits[index]);
}

const mapDispatchToProps = (dispatch: Dispatch) => {
    return {
        handleOnPageStart: (recruitID:string | undefined) => dispatch(Handlers.handleOnPageStart(recruitID)),
        handleOnPositionInputChanges: (value: string) => dispatch(Handlers.handleOnPositionInputChanges(value)),
        handleOnContentInputChanges: (value: string) => dispatch(Handlers.handleOnContentInputChanges(value)),
        handleOnQualificationInputChanges: (value: string) => dispatch(Handlers.handleOnQualificationInputChanges(value)),
        handleOnEmploymentStatusInputChanges: (value: string) => dispatch(Handlers.handleOnEmploymentStatusInputChanges(value)),
        handleOnCreateNewRecruit: () => dispatch(Handlers.handleOnCreateNewRecruit()),
        handleOnUpdateRecruit: () => dispatch(Handlers.handleOnUpdateRecruit()),
        handleOnDeleteRecruit: (recruitID:string | undefined) => dispatch(Handlers.handleOnDeleteRecruit(recruitID)),
        handleCancelDelete: () => dispatch(Handlers.handleCancelDelete()),
        handleDoDelete: (recruitID:string | null) => dispatch(Handlers.handleDoDelete(recruitID)),
    }
}

export const AdminRecruitEditPage = connect(mapStateToProps,mapDispatchToProps)(PageComponent);