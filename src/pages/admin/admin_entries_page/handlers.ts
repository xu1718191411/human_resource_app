import {GlobalService} from "../../../service/global_service";
import {Dispatch} from "redux";
import {getState} from "../../../stores/get_state";
import {parseEntries} from "../../../parsers/parsers";
import {EntitiesActions} from "../../../stores/actions";
import { AdminEntriesPageActions } from "../../../stores/actions/actions";
import {ItemsNumber} from "../../../constants/constants";
import {uniq} from "../../../utils/utils";
import {EntryEntity} from "../../../stores/states/common_states";
import {URLS} from "../../../configs";

export const onPageStart = ():any => async (dispatch:Dispatch, getState: getState, service:GlobalService) => {
    const isLogin = await service.firebaseService.firebaseAuthClient.checkISLogin();
    if (!isLogin) {
        service.history.push(URLS.URL_ADMIN_LOGIN);
        return;
    }
    const res = await service.firebaseService.getAllEntries()
    const result = parseEntries(res)
    dispatch(EntitiesActions.setEntries(result))
}

export const onItemsNumberChange = (value:string):any => async (dispatch:Dispatch, getState: getState, service:GlobalService) => {
    //ToDo check type here
    dispatch(AdminEntriesPageActions.setItemsNumber((parseInt(value) as ItemsNumber)));
}

export const onPageNumberChange = (value:number):any => async (dispatch:Dispatch, getState: getState, service:GlobalService) => {
    dispatch(AdminEntriesPageActions.setCurrentPageNumber(value));
}

export const onPrevClicked = ():any => async  (dispatch:Dispatch,getState: getState) => {
    const pageState = getState().pageStates.adminEntriesPageStates;
    if (pageState.currentPageNumber === 1) {
        return;
    }
    dispatch(AdminEntriesPageActions.setCurrentPageNumber(pageState.currentPageNumber-1));
}

export const onNextClicked = (totalPageNumber:number):any => async  (dispatch:Dispatch,getState: getState) => {
    const pageState = getState().pageStates.adminEntriesPageStates;
    if (pageState.currentPageNumber === totalPageNumber) {
        return;
    }
    dispatch(AdminEntriesPageActions.setCurrentPageNumber(pageState.currentPageNumber+1));
}


export const onSearchButtonClicked = (query:string):any => async (dispatch:Dispatch, getState: getState, service:GlobalService) => {
    if (query.length === 0) {
        dispatch(onPageStart());
        return;
    }
    const res = await service.firebaseService.queryEntries(query);
    const result = res.map(r => parseEntries(r));
    const ids = uniq(result.flat().map(e=>e.id));
    const tmp = ids.map(e=>result.flat().find(s=>s.id == e));
    const finalResult:EntryEntity[] = [];
    tmp.forEach(e=>{
        if (e !== undefined){
            finalResult.push(e);
        }
    })
    dispatch(EntitiesActions.setEntries(finalResult))

}

export const onQueryValueChanged = (query:string):any => async (dispatch:Dispatch, getState: getState, service:GlobalService) => {
    dispatch(AdminEntriesPageActions.setQueryValue(query));
}

export const handleDelete = (entryID:string):any => {
    return async(dispatch:Dispatch,getState:getState, service:GlobalService) => {
        dispatch(AdminEntriesPageActions.setDeletingID(entryID));
    }
}

export const handleCancelDelete = ():any => {
    return async(dispatch:Dispatch,getState:getState, service:GlobalService) => {
        dispatch(AdminEntriesPageActions.setDeletingID(null));
    }
}

export const handleDoDelete = (entryID:string | null):any => {
    return async(dispatch:Dispatch,getState:getState, service:GlobalService) => {
        if (entryID === null) {
            return;
        }
        await service.firebaseService.deleteEntry(entryID);
        dispatch(AdminEntriesPageActions.setDeletingID(null));
        service.history.push(URLS.URL_ADMIN_ENTRIES);
    }
}