import * as React from 'react';
import {AdminSideNavigation} from '../../../components/admin/admin_side_navigation';
import {PageNationComponent} from "../../../components/admin/pagination";
import {EntryProp} from "../../common_props";
import {GlobalStates} from "../../../stores/states/states";
import {Dispatch} from "redux";
import * as Handlers from "./handlers";
import {connect} from "react-redux";
import {mapDeleteEntry, mapEntriesStatesToProps} from "../../common_mapper";
import {Link} from "react-router-dom";
import {URLS} from "../../../configs";
import {ItemsNumber, PaginationItems} from "../../../constants/constants";
import {CSVLink} from "react-csv";
import {ConfirmEntryDeleteComponent} from "../../../components/admin/confirm_entry_delete_component";

export interface AdminEntriesPageProps {
    entries: EntryProp[];
    itemsNumber: ItemsNumber;
    totalPageNumber: number;
    currentPageNumber: number;
    queryValue: string;
    deletingEntry: EntryProp | null;
}

export interface AdminEntriesPageHandle {
    onPageStart(): void;

    onItemsNumberChange(value: string): void;

    handlePageItemClicked(value: number): void;

    handlePrevClicked(): void;

    handleNextClicked(totalPageNumber: number): void;

    handleSearchButtonClicked(query: string): void;

    handleQueryInputChange(value: string): void;

    handleDelete(entryID: string): void;

    handleCancelDelete(): void;

    handleDoDelete(entryID: string | null): void;
}

type Props = AdminEntriesPageProps & AdminEntriesPageHandle & History;

class PageComponent extends React.Component<Props> {
    componentDidMount(): void {
        this.props.onPageStart();
    }

    public render(): JSX.Element {
        return <div id="admin-entries" className="container-fluid">

            <div id={"admin-entries-body"} className={"row"}>

                <div className={"col-md-2 side-menu m-2 mb-12 card"}>
                    <AdminSideNavigation/>
                </div>

                <div className={"col-md m-2"}>

                    <div className={"row card mt-2 admin-title text-center"}>
                        <div className={"card-body"}>
                            <h3>エントリー一覧</h3>
                        </div>
                    </div>

                    <div className={"row card mt-2 admin-body"}>
                        <div className={"card-body"}>

                            <div className="row">
                                <div className={"col-md-9"}>
                                    <input type="email" className="form-control" placeholder="氏名、Email、IDから検索できます"
                                           onChange={e => this.props.handleQueryInputChange(e.target.value)}
                                           value={this.props.queryValue}/>
                                </div>
                                <div className={"col-md-3"}>
                                    <button onClick={_ => this.props.handleSearchButtonClicked(this.props.queryValue)}
                                            className={"btn btn-info col-6"}>検索
                                    </button>

                                    <button onClick={_ => this.props.onPageStart()}
                                            className={"btn btn-info ml-1"}>
                                        <CSVLink data={this.props.entries}>ダウンロード</CSVLink>
                                    </button>
                                </div>
                            </div>

                            <div className={"row mt-1"}>
                                <div className={"offset-md-9 col-md-3"}>
                                    <button className={"btn btn-info col-6"}>リロード</button>
                                    <select className="form-select ml-1" aria-label="Default select example"
                                            value={this.props.itemsNumber}
                                            onChange={e => this.props.onItemsNumberChange(e.target.value)}>
                                        {PaginationItems.map((p, k) => <option key={k}
                                                                               value={p.value}>{p.displayName}</option>)}
                                    </select>
                                </div>
                            </div>
                            <table className="table table-bordered align-middle mt-3">
                                <thead>
                                <tr>
                                    <th scope="col">ID</th>
                                    <th scope="col">氏名</th>
                                    <th scope="col">Email</th>
                                    <th scope="col">日付</th>
                                    <th scope="col">操作</th>
                                </tr>
                                </thead>
                                <tbody>

                                {this.props.entries.map((e, k) =>
                                    <tr key={k}>
                                        <th scope="row">{e.id}</th>
                                        <td>{e.name}</td>
                                        <td>{e.email}</td>
                                        <td>2020-12-20 20:08:02</td>
                                        <td>
                                            <button className={"btn btn-info"}><Link
                                                to={URLS.GET_URL_ADMIN_ENTRY_DETAIL(e.id)}>詳細</Link></button>
                                            <button className={"btn btn-danger ml-3"} onClick={_=>this.props.handleDelete(e.id)}>削除</button>
                                        </td>
                                    </tr>
                                )}
                                </tbody>
                            </table>

                            <PageNationComponent handleNextClicked={this.props.handleNextClicked}
                                                 handlePrevClicked={this.props.handlePrevClicked}
                                                 handlePageItemClicked={this.props.handlePageItemClicked}
                                                 currentPageNumber={this.props.currentPageNumber}
                                                 totalPageNumber={this.props.totalPageNumber}/>
                        </div>
                    </div>
                </div>

            </div>

            <ConfirmEntryDeleteComponent isShow={this.props.deletingEntry !== null}
                                         deletingEntry={this.props.deletingEntry}
                                         handleCancelDelete={this.props.handleCancelDelete}
                                         handleDoDelete={this.props.handleDoDelete}/>
        </div>
    }
}

const mapStateToProps = (globalStates: GlobalStates): AdminEntriesPageProps => {
    const pageState = globalStates.pageStates.adminEntriesPageStates;
    const totalItems = globalStates.states.entryEntities.length;
    return {
        entries: mapEntriesStatesToProps(globalStates.states.entryEntities).slice((pageState.currentPageNumber - 1) * pageState.itemsNumber, (pageState.currentPageNumber) * pageState.itemsNumber),
        itemsNumber: pageState.itemsNumber,
        totalPageNumber: (Math.ceil(totalItems / pageState.itemsNumber)),
        currentPageNumber: pageState.currentPageNumber,
        queryValue: pageState.queryValue,
        deletingEntry: mapDeleteEntry(pageState.deletingID, globalStates.states.entryEntities),
    }
}

const mapDispatchToProps = (dispatch: Dispatch) => {
    return {
        onPageStart: () => dispatch(Handlers.onPageStart()),
        onItemsNumberChange: (value: string) => dispatch(Handlers.onItemsNumberChange(value)),
        handlePageItemClicked: (value: number) => dispatch(Handlers.onPageNumberChange(value)),
        handlePrevClicked: () => dispatch(Handlers.onPrevClicked()),
        handleNextClicked: (totalPageNumber: number) => dispatch(Handlers.onNextClicked(totalPageNumber)),
        handleSearchButtonClicked: (query: string) => dispatch(Handlers.onSearchButtonClicked(query)),
        handleQueryInputChange: (value: string) => dispatch(Handlers.onQueryValueChanged(value)),
        handleDelete: (entryID: string) => dispatch(Handlers.handleDelete(entryID)),
        handleCancelDelete: () => dispatch(Handlers.handleCancelDelete()),
        handleDoDelete: (entryID: string | null) => dispatch(Handlers.handleDoDelete(entryID)),
    }
}

export const AdminEntriesPage = connect(mapStateToProps, mapDispatchToProps)(PageComponent);

