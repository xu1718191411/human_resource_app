import * as React from 'react';
import {GlobalStates} from "../../../stores/states/states";
import {Dispatch} from "redux";
import * as Handlers from './handlers';
import {connect} from "react-redux";


interface AdminLogoutPageHandlers {
    logout:()=>void;
}


interface AdminLogoutPageProps {

}

type Props = AdminLogoutPageHandlers & History & AdminLogoutPageProps

class Component extends React.Component<Props>{
    componentDidMount(): void {
        this.props.logout();
    }

    public render(): JSX.Element {
        return <div></div>;
    }
}

const mapStateToProps = (globalStates:GlobalStates):AdminLogoutPageProps => {
    return {}
}

const mapDispatchToProps = (dispatch:Dispatch):AdminLogoutPageHandlers => {
    return {
        logout: () => dispatch(Handlers.logout()),
    }
}

export const AdminLogOutPage = connect(mapStateToProps,mapDispatchToProps)(Component);