import {getState} from "../../../stores/get_state";
import {GlobalService} from "../../../service/global_service";
import {Dispatch} from "redux";
import {URLS} from "../../../configs";

export const logout = ():any => {
    return async (dispatch:Dispatch, getState:getState, service: GlobalService) => {
        await service.firebaseService.firebaseAuthClient.signOut();
        service.history.push(URLS.URL_ADMIN_LOGIN);
    }
}