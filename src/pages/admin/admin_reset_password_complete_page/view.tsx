import * as React from 'react';
import {RouteComponentProps} from "react-router";
import {Link} from "react-router-dom";
import {URLS} from "../../../configs";

export interface AdminResetPasswordCompletePageProps {
    email:string
}

type Props = AdminResetPasswordCompletePageProps & RouteComponentProps<{ email: string }>;


export class AdminResetPasswordCompletePage extends React.Component<Props>{
    public render(): JSX.Element {

        return <div id="admin-reset-password-complete" className="container-fluid">
                <div className={"row justify-content-center"}>
                    <div className="col-md-6 col-10 card login-body">
                        <div className="card-body">

                            <div className="mt-3 row justify-content-center">
                                <h5>{this.props.match.params.email}</h5>
                            </div>

                            <div className="mt-2 row justify-content-center">
                                <h5>上記のメールにパースワード再設定メールを送信しました。</h5>
                            </div>

                            <div className="mb-6 mt-6 row justify-content-center">
                                <div className="col-12 mt-3 col-md-7">
                                    <button className="col-12 btn btn-primary"><Link to={URLS.URL_ADMIN_LOGIN}>ログイン画面へ戻る</Link></button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
        </div>
    }
}