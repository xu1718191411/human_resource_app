import * as React from 'react';
import {GlobalStates} from "../../../stores/states/states";
import {Dispatch} from "redux";
import * as Handlers from './handlers';
import {connect} from "react-redux";

export interface AdminResetPasswordPageProps {
    email:string;
}

export interface AdminResetPasswordPageHandlers {
    handleEmailInput : (value:string) => void;
    handleResetPassword : () => void;
}


type Props = AdminResetPasswordPageProps & AdminResetPasswordPageHandlers & History;

export class Component extends React.Component<Props>{
    public render(): JSX.Element {

        return <div id="admin-reset-password" className="container-fluid">
                <div className={"row justify-content-center"}>
                    <div className="col-md-6 col-10 card login-body">
                        <div className="card-body">

                            <div className="mt-5 row justify-content-center">
                                <h5>パースワード再設定</h5>
                            </div>

                            <div className="mt-1 row justify-content-center">
                                <h5>登録されたメールアドレスを入力してください</h5>
                            </div>

                            <div className="mt-3 mb-3 row justify-content-center">
                                <div className="col-12 col-md-7">
                                    <input type="text" onChange={e=>this.props.handleEmailInput(e.target.value)} value={this.props.email}  className="form-control" placeholder={"メールアドレス"}/>
                                </div>
                            </div>

                            <div className="mb-3 row justify-content-center">
                                <div className="col-12 col-md-7">
                                    <button onClick={_=>this.props.handleResetPassword()} className="col-12 btn btn-primary">送信</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
        </div>
    }
}

const MapStateToProps = (globalStates:GlobalStates):AdminResetPasswordPageProps => {
    const pageState = globalStates.pageStates.adminResetPasswordStates;
    return {
        email:pageState.email,
    }
}

const MapDispatchToProps = (dispatch:Dispatch):AdminResetPasswordPageHandlers => {
    return {
        handleEmailInput: (value:string) => dispatch(Handlers.onEmailInput(value)),
        handleResetPassword: () => dispatch(Handlers.onSendRequest()),
    }
}

export const AdminResetPasswordPage = connect(MapStateToProps,MapDispatchToProps)(Component);