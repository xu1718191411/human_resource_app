import {Dispatch} from "redux";
import {getState} from "../../../stores/get_state";
import {GlobalService} from "../../../service/global_service";
import {AdminResetPasswordPageActions} from "../../../stores/actions/actions";
import {URLS} from "../../../configs";

export const onEmailInput = (value:string):any =>  {
    return  async (dispatch:Dispatch, getStates:getState, service:GlobalService) => {
        dispatch(AdminResetPasswordPageActions.setEmail(value));
    }
}

export const onSendRequest = ():any => {
    return async (dispatch:Dispatch, getStates:getState, service:GlobalService) => {
        const pageState = getStates().pageStates.adminResetPasswordStates;
        await service.firebaseService.firebaseAuthClient.resetPassword(pageState.email);
        service.history.push(URLS.GET_URL_RESET_PASSWORD_COMPLETE(pageState.email));
    }
}