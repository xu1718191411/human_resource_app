import {GlobalService} from "../../../service/global_service";
import {Dispatch} from "redux";
import {getState} from "../../../stores/get_state";
import {AdminLoginPageActions} from "../../../stores/actions/actions";
import {URLS} from "../../../configs";

export const handleUserNameChanges = (value:string):any => {
    return async (dispatch:Dispatch) => {
        dispatch(AdminLoginPageActions.setUserName(value));
    }
}

export const handlePassWordChanges = (value:string):any => {
    return async (dispatch:Dispatch) => {
        dispatch(AdminLoginPageActions.setPassWord(value));
    }
}

export const handleLogin = ():any => {
    return async (dispatch:Dispatch, getState:getState ,service:GlobalService) => {
        const pageState = getState().pageStates.adminLoginPageStates;
        const res = await service.firebaseService.firebaseAuthClient.loginWithMailAndPassword(pageState.userName,pageState.passWord);
        if (res.user !== null) {
            service.history.push(URLS.URL_ADMIN_ENTRIES);
        }
    }
}