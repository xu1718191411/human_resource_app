import * as React from 'react';
import {Link} from "react-router-dom";
import {GlobalStates} from "../../../stores/states/states";
import {Dispatch} from "redux";
import * as Handlers from './handlers';
import {connect} from "react-redux";
import {URLS} from "../../../configs";

interface AdminLoginPageProps {
    userName:string;
    password:string;
}

interface AdminLoginPageHandlers {
    handleUserNameChanges: (value:string) => void;
    handlePassWordChanges: (value:string) => void;
    handleLoginClicked: () => void;
}

type Props = AdminLoginPageProps & History & AdminLoginPageHandlers

export class PageComponent extends React.Component<Props>{
    public render(): JSX.Element {

        return <div id="admin-login" className="container-fluid">
                <div className={"row justify-content-center"}>
                    <div className="col-md-6 col-10 card login-body">
                        <div className="card-body">
                            <div className="mt-5 mb-3 row">
                                <label htmlFor="userName" className="col-12 col-md-2 offset-md-2 col-form-label">userName</label>
                                <div className="col-12 col-md-6">
                                    <input type="text" onChange={e=>this.props.handleUserNameChanges(e.target.value)} value={this.props.userName} className="form-control"/>
                                </div>
                            </div>

                            <div className="mb-3 row">
                                <label htmlFor="password" className="col-12 col-md-2 offset-md-2 col-form-label">Password</label>
                                <div className="col-12 col-md-6">
                                    <input type="password" className="form-control" onChange={e=>this.props.handlePassWordChanges(e.target.value)} value={this.props.password}/>
                                </div>
                            </div>

                            <div className="mb-3 row">
                                <label htmlFor="password" className="col-12 col-md-2 offset-md-2 col-form-label"></label>
                                <div className="col-12 col-md-6">
                                    <button className="col-12 btn btn-primary" onClick={this.props.handleLoginClicked}>ログイン</button>
                                </div>
                            </div>


                            <div className="mb-6 row">
                                <div className="col-12 offset-md-4 col-md-6">
                                    <Link to={URLS.URL_RESET_PASSWORD}>パースワード見つからない場合</Link>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
        </div>
    }
}

export const mapStatesToProps = (globalStates:GlobalStates):AdminLoginPageProps => {
    const pageState = globalStates.pageStates.adminLoginPageStates;

    return {
        userName:pageState.userName,
        password:pageState.passWord,
    }
}

export const mapDispatchToProps = (dispatch:Dispatch):AdminLoginPageHandlers => {
    return {
        handleUserNameChanges: (value:string) => dispatch(Handlers.handleUserNameChanges(value)),
        handlePassWordChanges: (value:string) => dispatch(Handlers.handlePassWordChanges(value)),
        handleLoginClicked: () => dispatch(Handlers.handleLogin()),
    }
}

export const AdminLoginPage = connect(mapStatesToProps,mapDispatchToProps)(PageComponent);