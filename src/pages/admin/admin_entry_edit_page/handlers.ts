import {GlobalService} from "../../../service/global_service";
import {getState} from "../../../stores/get_state";
import {Dispatch} from "redux";
import {parseEntry, parseRecruits} from "../../../parsers/parsers";
import {AdminEntryEditPageActions} from "../../../stores/actions/actions";
import {EntitiesActions} from "../../../stores/actions";
import {URLS} from "../../../configs";

export const onPageStart = (entryID:string):any => {
    return async (dispatch:Dispatch,getState:getState, service:GlobalService) => {
        const isLogin = await service.firebaseService.firebaseAuthClient.checkISLogin();
        if (!isLogin) {
            service.history.push(URLS.URL_ADMIN_LOGIN);
            return;
        }

        const res = await service.firebaseService.getEntry(entryID);
        const entry = parseEntry(res.data(),res.id);
        dispatch(AdminEntryEditPageActions.setEntryID(res.id));
        dispatch(AdminEntryEditPageActions.setName(entry.name));
        dispatch(AdminEntryEditPageActions.setEmail(entry.email));
        dispatch(AdminEntryEditPageActions.setAge(entry.age));
        dispatch(AdminEntryEditPageActions.setDesiredOccupation(entry.desiredOccupation));
        dispatch(AdminEntryEditPageActions.setReasonForApply(entry.reasonForApply));
        dispatch(EntitiesActions.addEntry(entry));

        const recruits = await service.firebaseService.getRecruits();
        const recruitsEntities = parseRecruits(recruits);
        dispatch(EntitiesActions.setRecruits(recruitsEntities));
    }
}

export const handleOnNameInputValueChanges = (value:string):any => {
    return async (dispatch:Dispatch,getState:getState, service:GlobalService) => {
        dispatch(AdminEntryEditPageActions.setName(value));
    }
}

export const handleEmailInputValueChanges = (value:string):any => {
    return async(dispatch:Dispatch,getState:getState, service:GlobalService) => {
        dispatch(AdminEntryEditPageActions.setEmail(value));
    }
}

export const handleAgeInputValueChanges = (value:string):any => {
    return async(dispatch:Dispatch,getState:getState, service:GlobalService) => {
        dispatch(AdminEntryEditPageActions.setAge(parseInt(value)));
    }
}

export const handleDesiredOccupationChanges = (value:string):any => {
    return async(dispatch:Dispatch,getState:getState, service:GlobalService) => {
        dispatch(AdminEntryEditPageActions.setDesiredOccupation(value));
    }
}

export const handleReasonForApply = (value:string):any => {
    return async(dispatch:Dispatch,getState:getState, service:GlobalService) => {
        dispatch(AdminEntryEditPageActions.setReasonForApply(value));
    }
}

export const handleUpdate = ():any => {
    return async(dispatch:Dispatch,getState:getState, service:GlobalService) => {
        const pageState = getState().pageStates.adminEntryEditPageStates;
        await service.firebaseService.updateEntry(pageState.entryID,pageState.editNameVal,pageState.editEmailVal,pageState.editAgeVal,pageState.editDesiredOccupationVal,pageState.editApplyReasonVal)
        service.history.push(URLS.URL_ADMIN_ENTRIES);
    }
}

export const handleDelete = (entryID:string):any => {
    return async(dispatch:Dispatch,getState:getState, service:GlobalService) => {
        dispatch(AdminEntryEditPageActions.setDeletingID(entryID));
    }
}

export const handleCancelDelete = ():any => {
    return async(dispatch:Dispatch,getState:getState, service:GlobalService) => {
        dispatch(AdminEntryEditPageActions.setDeletingID(null));
    }
}

export const handleDoDelete = (entryID:string | null):any => {
    return async(dispatch:Dispatch,getState:getState, service:GlobalService) => {
        if (entryID === null) {
            return;
        }
        await service.firebaseService.deleteEntry(entryID);
        dispatch(AdminEntryEditPageActions.setDeletingID(null));
        service.history.push(URLS.URL_ADMIN_ENTRIES);
    }
}