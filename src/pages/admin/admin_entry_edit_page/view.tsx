import * as React from 'react';
import {AdminSideNavigation} from "../../../components/admin/admin_side_navigation";
import {EntryProp, RecruitProp} from "../../common_props";
import {GlobalStates} from "../../../stores/states/states";
import {Dispatch} from "redux";
import * as Handlers from './handlers';
import {connect} from "react-redux";
import {Link, RouteComponentProps} from 'react-router-dom'
import {ageRange, customStyles} from "../../../constants/constants";
import { URL_ADMIN_ENTRIES } from "../../../configs/urls";
import Modal from 'react-modal';
import {EntryEntity} from "../../../stores/states/common_states";
import {mapDeleteEntry, mapEntryStateToProp, mapRecruitsStatesToProps} from "../../common_mapper";
import {ConfirmEntryDeleteComponent} from '../../../components/admin/confirm_entry_delete_component';

export interface AdminEntryEditPageProps {
    entryID: string,
    entryInfo: EntryProp,
    editNameVal: string;
    editEmailVal: string;
    editAgeVal: number;
    editDesiredOccupationVal: string;
    editApplyReasonVal: string;
    editStatusVal: string;
    deletingEntry: EntryProp | null;
    recruits: RecruitProp[];
}

interface AdminEntryEditPageHandlers {
    onPageStart(entryID: string): void;

    handleOnNameInputValueChanges(value: string): void

    handleEmailInputValueChanges(value: string): void

    handleAgeInputValueChanges(value: string): void

    handleDesiredOccupationChanges(value: string): void

    handleReasonForApply(value: string): void

    handleUpdate():void

    handleDelete(entryID:string):void;

    handleCancelDelete():void;

    handleDoDelete(entryID:string | null):void;
}


type Props = AdminEntryEditPageProps & History & AdminEntryEditPageHandlers & RouteComponentProps<{ entry_id: string }>;

export class PageComponent extends React.Component<Props> {

    componentDidMount(): void {
        this.props.onPageStart(this.props.match.params.entry_id);
    }

    public render(): JSX.Element {
        return <div id="admin-entry-edit" className="container-fluid">
            <div id={"admin-entries-body"} className={"row"}>

                <div className={"col-md-2 side-menu m-2 mb-12 card"}>
                    <AdminSideNavigation/>
                </div>

                <div className={"col-md m-2"}>
                    <div className={"row card mt-2 admin-title text-center"}>
                        <div className={"card-head"}>
                            <div className={"row col-10 justify-content-center"}>
                                <h3 className={"col-5"}>エントリー</h3>
                            </div>
                        </div>
                    </div>

                    <div className={"row card mt-2 admin-body"}>
                        <div className={"card-body"}>
                            <div className={"row"}>
                                <button type="button" className="btn btn-default col-1" aria-label="Left Align">
                                    <span className="glyphicon glyphicon-align-left" aria-hidden="true"><Link to={URL_ADMIN_ENTRIES}>←戻る</Link></span>
                                </button>
                            </div>

                            <div className={"row col-10 justify-content-center"}>
                                <div className={"row col-12 col-md-5 align-items-center"}>
                                    <label className="col-4 col-form-label">
                                        氏名
                                    </label>
                                    <div className="col-8 row">
                                        <input className={"col-12"} type="text" onChange={e=>this.props.handleOnNameInputValueChanges(e.target.value)} value={this.props.editNameVal}/>
                                    </div>
                                </div>
                            </div>

                            <div className={"row col-10 justify-content-center"}>
                                <div className={"row col-12 col-md-5 align-items-center"}>
                                    <label className="col-4 col-form-label">
                                        Email
                                    </label>
                                    <div className={"col-8 row"}>
                                        <input className={"col-12"} type="text" onChange={e=>this.props.handleEmailInputValueChanges(e.target.value)} value={this.props.editEmailVal} />
                                    </div>
                                </div>
                            </div>

                            <div className={"row col-10 justify-content-center"}>
                                <div className={"row col-12 col-md-5 align-items-center"}>
                                    <label className="col-4 col-form-label">
                                        年齢※
                                    </label>
                                    <div className={"col-8 row"}>
                                        <select onChange={e => this.props.handleAgeInputValueChanges(e.target.value)}
                                                value={this.props.editAgeVal} className="col-12 form-select"
                                                aria-label="Default select example">
                                            {ageRange.map(e => <option key={e} value={e}>{e}</option>)}
                                        </select>
                                    </div>
                                </div>
                            </div>


                            <div className={"row col-10 justify-content-center"}>
                                <div className={"row col-12 col-md-5 align-items-center"}>
                                    <label className="col-4 col-form-label">
                                        希望職種※
                                    </label>
                                    <div className="col-8 row">
                                        <select className="col-12 form-select" aria-label="Default select example"　onChange={e=>this.props.handleDesiredOccupationChanges(e.target.value)} value={this.props.editDesiredOccupationVal}>
                                            {this.props.recruits.map((e, k) => <option value={e.position} key={k}>{e.position}</option>)}
                                        </select>

                                    </div>
                                </div>
                            </div>

                            <div className={"row col-10 justify-content-center"}>
                                <div className={"row col-12 col-md-5 align-items-center"}>
                                    <label className="col-4 col-form-label">
                                        希望理由※
                                    </label>
                                    <div className="col-8 row">
                                        <textarea className="form-control" rows={5} onChange={e=>this.props.handleReasonForApply(e.target.value)} value={this.props.editApplyReasonVal}></textarea>
                                    </div>
                                </div>
                            </div>


                            <div className={"row col-10 justify-content-center"}>
                                <div className={"row col-12 col-md-5 align-items-center"}>
                                    <label className="col-4 col-form-label">
                                        ステータス
                                    </label>
                                    <div className="col-8 row">
                                        <select className="col-12 form-select" aria-label="Default select example">
                                            <option>受付</option>
                                            <option>受付済み</option>
                                            <option>採用済み</option>
                                        </select>
                                    </div>
                                </div>
                            </div>

                            <div className={"row col-10 justify-content-center mt-3"}>
                                <div className={"row col-12 col-md-5 align-items-center"}>
                                    <label className="col-4 col-form-label">

                                    </label>
                                    <div className="col-8 row">
                                        <button onClick={e=>this.props.handleUpdate()}  className={"btn btn-info col-5 mr-3"}>更新</button>
                                        <button onClick={e=>this.props.handleDelete(this.props.entryID)} className={"btn btn-danger col-5"}>削除</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <ConfirmEntryDeleteComponent isShow={this.props.deletingEntry !== null} deletingEntry={this.props.deletingEntry} handleCancelDelete={this.props.handleCancelDelete} handleDoDelete={this.props.handleDoDelete} />

                </div>

            </div>

        </div>;
    }
}


const mapStatesToProps = (globalStates: GlobalStates): AdminEntryEditPageProps => {
    const pageState = globalStates.pageStates.adminEntryEditPageStates;
    const entries = globalStates.states.entryEntities;
    const recruitsEntities = globalStates.states.recruits;
    //ToDo make it into a model
    const target = entries.find(e => e.id === pageState.entryID);

    let entryInfo: EntryProp = {
        id: '',
        name: '',
        email: '',
        age: 0,
        desiredOccupation: '',
        reasonForApply: '',
    }

    if (target !== undefined) {
        entryInfo = {
            id: target.id,
            name: target.name,
            email: target.email,
            age: target.age,
            desiredOccupation: target.desiredOccupation,
            reasonForApply: target.reasonForApply
        }
    }

    return {
        entryInfo: entryInfo,
        entryID: pageState.entryID,
        editNameVal: pageState.editNameVal,
        editEmailVal: pageState.editEmailVal,
        editAgeVal: pageState.editAgeVal,
        editDesiredOccupationVal: pageState.editDesiredOccupationVal,
        editApplyReasonVal: pageState.editApplyReasonVal,
        editStatusVal: '',
        deletingEntry: mapDeleteEntry(pageState.deletingID, entries),
        recruits: mapRecruitsStatesToProps(recruitsEntities),
    }
}

const mapDispatchToProps = (dispatch: Dispatch): AdminEntryEditPageHandlers => ({
    onPageStart: (entryID: string) => dispatch(Handlers.onPageStart(entryID)),

    handleOnNameInputValueChanges: (value: string) =>  dispatch(Handlers.handleOnNameInputValueChanges(value)),

    handleEmailInputValueChanges: (value: string) => dispatch(Handlers.handleEmailInputValueChanges(value)),

    handleAgeInputValueChanges: (value: string) => dispatch(Handlers.handleAgeInputValueChanges(value)),

    handleDesiredOccupationChanges: (value: string) => dispatch(Handlers.handleDesiredOccupationChanges(value)),

    handleReasonForApply: (value: string) => dispatch(Handlers.handleReasonForApply(value)),

    handleUpdate: () => dispatch(Handlers.handleUpdate()),

    handleDelete: (entryID:string) => dispatch(Handlers.handleDelete(entryID)),

    handleCancelDelete: () => dispatch(Handlers.handleCancelDelete()),

    handleDoDelete: (entryID:string | null) => dispatch(Handlers.handleDoDelete(entryID)),
})

export const AdminEntryEditPage = connect(mapStatesToProps, mapDispatchToProps)(PageComponent)