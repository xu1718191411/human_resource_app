import {EntryEntity, RecruitEntity} from "../stores/states/common_states";
import {EntryProp, RecruitProp} from "./common_props";

export const mapEntriesStatesToProps = (entries: EntryEntity[]): EntryProp[] => {
    return entries.map(e => (mapEntryStateToProp(e)));
}

export const mapEntryStateToProp = (e: EntryEntity): EntryProp => {
    return {
        id: e.id,
        name: e.name,
        email: e.email,
        age: e.age,
        desiredOccupation: e.desiredOccupation,
        reasonForApply: e.reasonForApply,
    }
}

export const mapRecruitsStatesToProps = (recruits: RecruitEntity[]): RecruitProp[] => {
    return recruits.map(r => mapRecruitStateToProp((r)));
}

export const mapRecruitStateToProp = (e: RecruitEntity): RecruitProp => {
    return {
        id: e.id,
        position: e.position,
        content: e.content,
        qualification: e.qualification,
        employment_status: e.employment_status,
    }
}

export const mapDeleteEntry = (entryID:string | null, entryEntities:EntryEntity[]): (EntryProp| null) => {
    if (entryEntities.length === 0) {
        return null;
    }

    const index = entryEntities.findIndex(e => e.id === entryID);

    if (index === -1) {
        return null
    }

    return mapEntryStateToProp(entryEntities[index]);
}