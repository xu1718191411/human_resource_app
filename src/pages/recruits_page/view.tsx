import * as React from 'react';
import {NavigationComponent} from "../../components/navigation_component";
import {RecruitItemComponent} from '../../components/recruit-item';
import {RecruitProp} from "../common_props";
import {GlobalStates} from "../../stores/states/states";
import {Dispatch} from "redux";
import {connect} from "react-redux";
import * as Handlers from './handlers';
import {mapRecruitsStatesToProps} from "../common_mapper";
import {FooterComponent} from "../../components/footer_component";

export interface RecruitPageProps {
    recruits:RecruitProp[];
}

interface RecruitsPageHandlers{
    onPageStart: ()=>void;
    handleNavigationClicked: (position:string)=> void;
    goToEntry: () => void;
}

type Props = RecruitPageProps & RecruitsPageHandlers & History



export class Component extends React.Component<Props>{

    componentDidMount(): void {
        this.props.onPageStart();
    }

    public render(): JSX.Element {
        return <div id="recruit" className="container-fluid">
            <NavigationComponent/>

            <div className={"row justify-content-center recruit-header p-3 border-bottom"}>
                <div className={"row col-10 text-center mt-1"}>
                    <h3 className={"col-12"}>募集項目</h3>
                </div>

                <div className={"row col-10 text-center"}>
                    {this.props.recruits.map((e,k) => <div role={"button"} key={k} className={"col"} onClick={_=>this.props.handleNavigationClicked(e.position)}>
                        {/*<HashLink to={"#" + e.position}></HashLink>*/}
                        {e.position}
                    </div>)}
                </div>
            </div>

            {this.props.recruits.map((e,k) => <RecruitItemComponent key={k}  id={e.position} position={e.position} content={e.content} qualification={e.qualification} employmentStatus={e.employment_status} />)}

            <div className={"row justify-content-center mt-3 mb-6"}>
                <button onClick={e=>this.props.goToEntry()} className={"col-5 btn btn-primary"}>
                    応募フォームへ
                </button>
            </div>

            <FooterComponent/>

        </div>
    }
}

const mapStatesToProps = (globalStates:GlobalStates):RecruitPageProps => {
    return {
        recruits:mapRecruitsStatesToProps(globalStates.states.recruits),
    }
}

const mapDispatchToProps = (dispatch:Dispatch):RecruitsPageHandlers => {
    return {
        onPageStart: () =>  dispatch(Handlers.onPageStart()),
        handleNavigationClicked: (position:string) => dispatch(Handlers.handleNavigationClicked(position)),
        goToEntry: () => dispatch(Handlers.goToEntry())
    }
}

export const RecruitsPage = connect(mapStatesToProps,mapDispatchToProps)(Component);
