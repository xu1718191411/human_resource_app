import {getState} from "../../stores/get_state";
import {GlobalService} from "../../service/global_service";
import {Dispatch} from "redux";
import {EntitiesActions} from "../../stores/actions";
import {parseRecruits} from "../../parsers/parsers";
import {URLS} from "../../configs";

export const onPageStart = ():any => {
    return async (dispatch:Dispatch, getState:getState, service:GlobalService) => {
        console.log("onPageStart");
        const recruits = await service.firebaseService.getRecruits();
        await dispatch(EntitiesActions.setRecruits(parseRecruits(recruits)));
    }
}

export const handleNavigationClicked = (position:string):any => {
    return async (dispatch:Dispatch, getState:getState, service:GlobalService) => {
        console.log("handleNavigationClicked");
        service.history.push(URLS.URL_RECRUIT + "#" + position);
    }
}


export const goToEntry = ():any => {
    return async (dispatch:Dispatch, getState:getState, service:GlobalService) => {
        service.history.push(URLS.URL_ENTRY);
    }
}





