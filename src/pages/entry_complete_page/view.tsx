import * as React from 'react';
import {NavigationComponent} from "../../components/navigation_component";

export interface EntryCompleteProps {
    title:string;
}

type Props = EntryCompletePage & History;


export class EntryCompletePage extends React.Component<Props>{
    public render(): JSX.Element {
        return <div id="entry-complete" className="container-fluid">
            <NavigationComponent/>

            <div className="card">
                <div className="card-body">
                    <div className={"row justify-content-center"}>
                        <div className={"align-items-center text-center title"}>
                            <h5>エントリーしていただき、ありがとうございました</h5>
                            <h6>確認メールを入力いただいたメールアドレスに送信しました。</h6>
                            <h6>人事からの連絡をしばらくお待ちください。</h6>
                        </div>
                    </div>

                    <div className={"row item justify-content-center back-button"}>
                        <div className={"row col-md-4 col-6"}>
                            <button type="button" className="col-12 btn btn-secondary center-block">戻る</button>
                        </div>
                    </div>

                </div>
            </div>
        </div>
    }
}