export interface EntryProp {
    id: string;
    name: string;
    email: string;
    age: number;
    desiredOccupation: string;
    reasonForApply: string;
}


export interface RecruitProp {
    id: string;
    position: string;
    content: string;
    qualification: string;
    employment_status: string;
}