import * as React from 'react';
import {Link} from "react-router-dom";
import {URL_ADMIN_ENTRIES, URL_ADMIN_RECRUIT_CREATE, URL_ADMIN_RECRUITS} from "../../configs/urls";
import {URLS} from "../../configs";

export class AdminSideNavigation extends React.Component{
    public render(): JSX.Element {
        return <div className="card-body">
            <ul className="nav flex-column">
                <li className="nav-item">
                    <button className="btn col-12 btn-light nav-link active" aria-current="page"><Link to={URL_ADMIN_ENTRIES}>エントリー一覧</Link></button>
                    <div>
                        <ul className="list-unstyled fw-normal pb-1 ">
                            <li>
                                <button className="mt-1 btn col-12 btn-light nav-link active text-right rounded">エントリー詳細</button>
                            </li>
                        </ul>
                    </div>
                </li>
                <li className="nav-item">
                    <button className="nav-link btn btn-light col-12 active" ><Link to={URL_ADMIN_RECRUITS}>応募職種一覧</Link></button>
                    <div>
                        <ul className="list-unstyled fw-normal pb-1 ">
                            <li><button
                                className="mt-1 btn btn-light active col-12 nav-link active text-right rounded">職種詳細</button>
                            </li>
                        </ul>
                    </div>
                </li>

                <li className="nav-item">
                    <button className="btn btn-light nav-link active col-12"><Link to={URL_ADMIN_RECRUIT_CREATE}>新規応募職種</Link></button>
                </li>


                <li className="nav-item text-center mt-xl-3">
                    <button className="btn btn-light nav-link active col-12"><Link to={URLS.URL_ADMIN_LOGOUT}>ログアウト</Link></button>
                </li>


            </ul>

        </div>;
    }
}