import * as React from 'react';
import {customStyles} from "../../constants/constants";
import Modal from "react-modal";
import {EntryProp} from "../../pages/common_props";

export interface ConfirmEntryDeleteComponentProps {
    isShow: boolean;
    deletingEntry: EntryProp | null,
    handleDoDelete: (entryID: string | null) => void;
    handleCancelDelete: () => void;
}

export class ConfirmEntryDeleteComponent extends React.Component<ConfirmEntryDeleteComponentProps> {
    render(): JSX.Element {
        return <Modal isOpen={this.props.deletingEntry !== null} style={customStyles}>
            <div className="container-fluid">
                <div className={"row justify-content-center col-12"}>
                    <div className="alert alert-danger col-10 text-center" role="alert">
                        この応募者の情報を削除しますか?
                    </div>

                    <div className={"col-10"}>
                        <table className="table table-bordered">
                            <tbody>
                            <tr>
                                <th scope="row">ID</th>
                                <td>{this.props.deletingEntry && this.props.deletingEntry.id}</td>
                            </tr>
                            <tr>
                                <th scope="row">氏名</th>
                                <td>{this.props.deletingEntry && this.props.deletingEntry.name}</td>
                            </tr>
                            <tr>
                                <th scope="row">Email</th>
                                <td>{this.props.deletingEntry && this.props.deletingEntry.email}</td>
                            </tr>
                            </tbody>
                        </table>
                    </div>

                    <div className={"row col-10 col-md-7"}>
                        <div className={"col"}>
                            <button type="button" className="col-10 btn btn-primary"
                                    onClick={_ => this.props.handleCancelDelete()}>キャンセル
                            </button>
                        </div>
                        <div className={"col"}>
                            <button type="button" className="col-10 btn btn-danger"
                                    onClick={_ => this.props.handleDoDelete(this.props.deletingEntry === null ? null : this.props.deletingEntry.id)}>削除
                            </button>
                        </div>
                    </div>
                </div>
            </div>
        </Modal>;
    }
}