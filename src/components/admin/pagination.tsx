import * as React from 'react';
import {range} from "../../utils/utils";

export interface PageNationComponentProps {
    totalPageNumber:number;
    currentPageNumber:number;
    handlePageItemClicked(pageNumber:number):void
    handlePrevClicked():void
    handleNextClicked(totalPageNumber:number):void
}

export class PageNationComponent extends React.Component<PageNationComponentProps>{
    public render() {
        return (
            <div className={"row justify-content-center"}>
                <nav aria-label="Page navigation example">
                    <ul className="pagination">
                        <li className="page-item">
                            <span onClick={_=>this.props.handlePrevClicked()} role='button' className="page-link" aria-label="Previous">
                                <span aria-hidden="true">&laquo;</span>
                            </span>
                        </li>
                        {range(1,this.props.totalPageNumber).map((v,k)=><li key={k} className={this.isActiveClass(v)} onClick={_ => this.props.handlePageItemClicked(v)}><span role='button' className="page-link" key={k}>{v}</span></li>)}
                        <li className="page-item" onClick={_=>this.props.handleNextClicked(this.props.totalPageNumber)}>
                            <span role='button' className="page-link" aria-label="Next">
                                <span aria-hidden="true">&raquo;</span>
                            </span>
                        </li>
                    </ul>
                </nav>
            </div>
        );
    }

    private isActiveClass(value:number):string{
        if (this.props.currentPageNumber === value) {
            return 'page-item active';
        }
        return 'page-item';
    }

}