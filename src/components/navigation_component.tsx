import * as React from 'react';
import {Link} from "react-router-dom";
import {URLS} from "../configs";
export class NavigationComponent extends React.Component{
    public render(): JSX.Element {
        return <div className={"row"}>
            <nav className="navbar navbar-expand-lg navbar-light bg-light col-12">
                <div className="container-fluid">
                    <a className="navbar-brand" href="#">株式会社XXX</a>
                    <button className="navbar-toggler" type="button" data-bs-toggle="collapse"
                            data-bs-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false"
                            aria-label="Toggle navigation">
                        <span className="navbar-toggler-icon"></span>
                    </button>
                    <div className="collapse navbar-collapse justify-content-end">
                        <ul className="navbar-nav">
                            <li className="nav-item">
                                <Link to={URLS.URL_HOME} className="nav-link active" aria-current="page">ホーム</Link>
                            </li>
                            <li className="nav-item">
                                <Link to={URLS.URL_ABOUT} className="nav-link">会社概要</Link>
                            </li>
                            <li className="nav-item">
                                <Link to={URLS.URL_NEWS} className="nav-link">最新情報</Link>
                            </li>
                            <li className="nav-item">
                                <Link to={URLS.URL_RECRUIT} className="nav-link">採用情報</Link>
                            </li>
                            <li className="nav-item">
                                <Link to={URLS.URL_CONTACT} className="nav-link">問合せ</Link>
                            </li>
                        </ul>
                    </div>
                </div>
            </nav>
        </div>;
    }
}