import * as React from 'react';
import {URLS} from "../configs";
import {Link} from "react-router-dom";

export class FooterComponent extends React.Component {
    render(): JSX.Element {
        return <div className={"row justify-content-center mt-4 mb-5 border-top"}>
            <div className={"row col-10 text-center mt-1"}>
                <div role={"button"} className={"col"}><Link role={"button"} to={URLS.URL_HOME}>Home</Link></div>
                <div role={"button"} className={"col"}><Link role={"button"} to={URLS.URL_ABOUT}>About</Link></div>
                <div role={"button"} className={"col"}><Link role={"button"} to={URLS.URL_NEWS}>News</Link></div>
                <div role={"button"} className={"col"}><Link role={"button"} to={URLS.URL_RECRUIT}>Recruit</Link></div>
                <div role={"button"} className={"col"}><Link role={"button"} to={URLS.URL_CONTACT}>Contact</Link></div>
            </div>
            <br/><br/>
        </div>;
    }
}