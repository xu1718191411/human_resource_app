import * as React from 'react';

export interface RecruitItemProps {
    id:string;
    position:string;
    content: string;
    qualification: string;
    employmentStatus: string;
}

export class RecruitItemComponent extends React.Component<RecruitItemProps> {
    public render(): JSX.Element {
        const props = this.props;
        return <div className={"row justify-content-center mt-3 recruit-item"}>
            <div className={"col-md-6 col-10"}>
                <div className="card" id={props.position}>
                    <h5 className="card-header">{props.position}</h5>
                    <div className="card-body">
                        <div className={"row border-bottom"}>
                            <div className={"col-3"}>仕事内容</div>
                            <div className={"col-9"}>{props.content}
                            </div>
                        </div>

                        <div className={"row mt-3 border-bottom"}>
                            <div className={"col-3"}>応募資格</div>
                            <div
                                className={"col-9"}>{props.qualification}
                            </div>
                        </div>

                        <div className={"row mt-3"}>
                            <div className={"col-3"}>雇用形態</div>
                            <div
                                className={"col-9"}>{props.employmentStatus}
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        </div>;
    }
}