import {EntryPageState} from '../../states/entry_page/state';
import {reducerWithInitialState} from "typescript-fsa-reducers";
import {EntryPageActions} from "../../actions/actions";
export const initState:EntryPageState = {
    name: '',
    email: '',
    age: 20,
    desiredOccupation: '',
    reasonForApply: '',
    formError: null,
}

export const EntryPageReducerReducer = reducerWithInitialState(initState)
    .case(EntryPageActions.setName, (state, payload) => {
        return { ...state, name: payload }
    })
    .case(EntryPageActions.setEmail, (state, payload) => {
        return { ...state, email: payload }
    })
    .case(EntryPageActions.setAge, (state,payload) => {
        return { ...state, age: payload}
    })
    .case(EntryPageActions.setDesiredOccupation,(state, payload) => {
        return { ...state, desiredOccupation: payload}
    })
    .case(EntryPageActions.setReasonForApply, (state, payload) => {
        return { ...state, reasonForApply: payload}
    })
    .case(EntryPageActions.setFormError, (state, payload) => ({
        ...state,formError:payload,
    }))