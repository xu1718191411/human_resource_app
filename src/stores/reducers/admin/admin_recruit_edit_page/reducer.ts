import {reducerWithInitialState} from "typescript-fsa-reducers";
import {AdminRecruitEditPageStates} from "../../../states/admin/admin_page_states";
import {AdminRecruitEditPageActions} from "../../../actions/actions";

const initState: AdminRecruitEditPageStates = {
    recruitID: '',
    position: '',
    content: '',
    qualification: '',
    employmentStatus: '',
    deletingID: null,
}

export const adminRecruitEditPageReducer = reducerWithInitialState(initState)
    .case(AdminRecruitEditPageActions.setRecruitID, (state, payload) => ({
        ...state, recruitID: payload,
    })).case(AdminRecruitEditPageActions.setPosition, (state, payload) => ({
        ...state, position: payload,
    })).case(AdminRecruitEditPageActions.setContent, (state, payload) => ({
        ...state, content: payload,
    })).case(AdminRecruitEditPageActions.setQualification, (state, payload) => ({
        ...state, qualification: payload,
    })).case(AdminRecruitEditPageActions.setEmploymentStatus, (state, payload) => ({
        ...state, employmentStatus: payload,
    })).case(AdminRecruitEditPageActions.setDeletingID, (state, payload) => ({
        ...state, deletingID: payload,
    }))