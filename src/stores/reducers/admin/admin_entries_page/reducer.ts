import {AdminEntriesPageStates} from "../../../states/admin/admin_page_states";
import {reducerWithInitialState} from "typescript-fsa-reducers";
import {AdminEntriesPageActions} from "../../../actions/actions";


export const initAdminEntriesPageState: AdminEntriesPageStates = {
    title: '',
    detailDisplay: false,
    itemsNumber: 5,
    currentPageNumber:1,
    queryValue:'',
    deletingID: null,
}

export const AdminEntriesPageReducer = reducerWithInitialState(initAdminEntriesPageState).case(AdminEntriesPageActions.setDetailDisplay, (state, payload) => ({
    ...state, detailDisplay: !state.detailDisplay,
})).case(AdminEntriesPageActions.setItemsNumber,(state, payload) => ({
    ...state, itemsNumber: payload,
})).case(AdminEntriesPageActions.setCurrentPageNumber, (state, payload) => ({
    ...state,currentPageNumber: payload
})).case(AdminEntriesPageActions.setQueryValue,(state, payload) => ({
    ...state,queryValue: payload,
})).case(AdminEntriesPageActions.setDeletingID,(state, payload) => ({
    ...state,deletingID: payload,
}));