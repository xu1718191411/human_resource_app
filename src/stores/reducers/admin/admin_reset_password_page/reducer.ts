import {AdminResetPasswordPageStates} from "../../../states/admin/admin_reset_password_page_states";
import {reducerWithInitialState} from "typescript-fsa-reducers";
import {AdminResetPasswordPageActions} from "../../../actions/actions";

const initPageState:AdminResetPasswordPageStates = {
    email:'',
}

export const adminResetPasswordPageReducer = reducerWithInitialState<AdminResetPasswordPageStates>(initPageState)
    .case(AdminResetPasswordPageActions.setEmail,(state, payload) => ({
        ...state, email: payload,
    }))