import {AdminEntryEditPageStates} from "../../../states/admin/admin_page_states";
import {reducerWithInitialState} from "typescript-fsa-reducers";
import {AdminEntryEditPageActions} from "../../../actions/actions";

export const initAdminEntryEditPageState: AdminEntryEditPageStates = {
    entryID: '',
    editNameVal: '',
    editEmailVal: '',
    editAgeVal: 0,
    editDesiredOccupationVal: '',
    editApplyReasonVal: '',
    editStatusVal: '',
    deletingID: null,
}

export const adminEntryEditPageReducer = reducerWithInitialState(initAdminEntryEditPageState).case(AdminEntryEditPageActions.setEntryID, (state, payload) => ({
    ...state, entryID: payload,
})).case(AdminEntryEditPageActions.setName, (state, payload) => {
    return {...state, editNameVal: payload}
})
    .case(AdminEntryEditPageActions.setEmail, (state, payload) => {
        return {...state, editEmailVal: payload}
    })
    .case(AdminEntryEditPageActions.setAge, (state, payload) => {
        return {...state, editAgeVal: payload}
    })
    .case(AdminEntryEditPageActions.setDesiredOccupation, (state, payload) => {
        return {...state, editDesiredOccupationVal: payload}
    })
    .case(AdminEntryEditPageActions.setReasonForApply, (state, payload) => {
        return {...state, editApplyReasonVal: payload}
    })
    .case(AdminEntryEditPageActions.setDeletingID,(state, payload) => ({
        ...state, deletingID:payload,
    }));