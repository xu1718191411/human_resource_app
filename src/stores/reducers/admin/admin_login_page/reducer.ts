import {AdminLoginPageStates} from "../../../states/admin/admin_login_page_states";
import {reducerWithInitialState} from "typescript-fsa-reducers";
import {AdminLoginPageActions} from "../../../actions/actions";

const initPageState: AdminLoginPageStates = {
    userName: '',
    passWord: '',
}

export const adminLoginPageReducer = reducerWithInitialState<AdminLoginPageStates>(initPageState).case(AdminLoginPageActions.setUserName, (state, payload) => ({
    ...state, userName: payload,
})).case(AdminLoginPageActions.setPassWord, (state, payload) => ({
    ...state, passWord: payload,
}))