import {AdminRecruitsPageStates} from "../../../states/admin/admin_recruits_page_states";
import {reducerWithInitialState} from "typescript-fsa-reducers";

const initAdminRecruitsPageState:AdminRecruitsPageStates = {
    title:'',
}

export const adminRecruitsPageReducer = reducerWithInitialState(initAdminRecruitsPageState)