import { EntryConfirmPageState } from "../../states/entry_complete_page/state";
import { reducerWithInitialState } from "typescript-fsa-reducers";
import { EntryConfirmPageActions } from "../../actions/actions";

export const entryConfirmPageState:EntryConfirmPageState = {
    applyEntryID:'',
}

export const EntryConfirmPageReducerReducer = reducerWithInitialState(entryConfirmPageState).case(EntryConfirmPageActions.setEntryApplyID,(state, payload) => ({
    ...state,applyEntryID:payload,
}))