import {combineReducers, createStore, compose, applyMiddleware, Store} from 'redux'
import {pageReducer, Reducer} from './reducer'
import {GlobalStates} from './states/states';
import thunk from "redux-thunk"
import {createGlobalService} from "../service/create_global_service";
import { routerMiddleware } from 'connected-react-router'
import {FirebaseConfig} from "../service/firebase_service";

const storeEnhancers = (window as any).__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose;


export const createReduxStore = (history:any,firebaseConfig:FirebaseConfig):Store => {
    const store = createStore(
        combineReducers<GlobalStates>({states:Reducer,pageStates:pageReducer}),
        storeEnhancers(applyMiddleware(thunk.withExtraArgument(createGlobalService(firebaseConfig,history)),routerMiddleware(history)))
    )
    return store;
}