import {GlobalStates} from "./states/states";

export type getState = () => GlobalStates;