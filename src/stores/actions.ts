import { actionCreatorFactory } from 'typescript-fsa';
import {EntryEntity, RecruitEntity} from "./states/common_states";

const actionCreator = actionCreatorFactory()

export const EntitiesActions = {
    setEntries: actionCreator<EntryEntity[]>('SET_ENTRIES'),
    addEntry: actionCreator<EntryEntity>('ADD_ENTRY'),
    setRecruits: actionCreator<RecruitEntity[]>('SET_RECRUITS'),
    addRecruit: actionCreator<RecruitEntity>('ADD_RECRUIT'),
    setApplyEntryEntity: actionCreator<EntryEntity | null>('Set_Apply_Entry_Entity'),
}