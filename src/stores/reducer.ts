import {reducerWithInitialState} from 'typescript-fsa-reducers'
import {EntitiesActions} from "./actions";
import {PageStates, States} from './states/states';
import {combineReducers} from "redux";
import {EntryPageReducerReducer} from "./reducers/entry_page/reducer";
import {AdminEntriesPageReducer} from "./reducers/admin/admin_entries_page/reducer";
import {adminEntryEditPageReducer} from "./reducers/admin/admin_entry_edit_page/reducer";
import {adminRecruitEditPageReducer} from "./reducers/admin/admin_recruit_edit_page/reducer";
import {adminRecruitsPageReducer} from "./reducers/admin/admin_recruits_page/reducer";
import {adminLoginPageReducer} from "./reducers/admin/admin_login_page/reducer";
import {EntryConfirmPageReducerReducer} from "./reducers/entry_complete_page/reducer";
import {adminResetPasswordPageReducer} from "./reducers/admin/admin_reset_password_page/reducer";

export const initialState: States = {
    inputValue: '',
    selectedValue: '',
    entryEntities: [],
    applyEntryEntity: null,
    recruits: [],
}

export const Reducer = reducerWithInitialState(initialState)
    .case(EntitiesActions.setEntries, (state, payload) => ({
        ...state, entryEntities: payload
    }))
    .case(EntitiesActions.addEntry, (state, payload) => ({
        ...state, entryEntities: state.entryEntities.filter(e => e.id !== payload.id).concat(payload)
    }))
    .case(EntitiesActions.setRecruits, (state, payload) => ({
        ...state, recruits: payload
    }))
    .case(EntitiesActions.addRecruit, (state, payload) => ({
        ...state, recruits: state.recruits.filter(e => e.id !== payload.id).concat(payload)
    }))
    .case(EntitiesActions.setApplyEntryEntity, (state, payload) => ({
        ...state, applyEntryEntity: payload,
    }))


export const pageReducer = combineReducers<PageStates>({
    entryPageState: EntryPageReducerReducer,
    entryConfirmPage: EntryConfirmPageReducerReducer,
    adminEntriesPageStates: AdminEntriesPageReducer,
    adminEntryEditPageStates: adminEntryEditPageReducer,
    adminRecruitEditPageStates: adminRecruitEditPageReducer,
    adminRecruitsPageStates: adminRecruitsPageReducer,
    adminLoginPageStates: adminLoginPageReducer,
    adminResetPasswordStates: adminResetPasswordPageReducer,
})