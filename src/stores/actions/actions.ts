import {actionCreatorFactory} from 'typescript-fsa';
import {ItemsNumber} from "../../constants/constants";

const actionCreator = actionCreatorFactory()

export const EntryPageActions = {
    setName: actionCreator<string>('ENTRY_PAGE_SET_NAME'),
    setEmail: actionCreator<string>('ENTRY_PAGE_SET_EMAIL'),
    setAge: actionCreator<number>('ENTRY_PAGE_SET_AGE'),
    setDesiredOccupation: actionCreator<string>('ENTRY_PAGE_SET_DESIRED_OCCUPATION'),
    setReasonForApply: actionCreator<string>('ENTRY_PAGE_SET_REASON_FOR_APPLY'),
    setFormError: actionCreator<string | null>('ENTRY_PAGE_SET_FORM_ERROR'),
}

export const EntryConfirmPageActions = {
    setEntryApplyID: actionCreator<string>('ENTRY_CONFIRM_PAGE_SET_ENTRY_APPLY_ID'),
}

export const AdminEntriesPageActions = {
    setDetailDisplay: actionCreator<void>('ADMIN_ENTRIES_PAGE_SET_DETAIL_DISPLAY'),
    setItemsNumber:actionCreator<ItemsNumber>('ADMIN_ENTRIES_PAGE_SET_ITEMS_NUMBER'),
    setCurrentPageNumber: actionCreator<number>('ADMIN_ENTRIES_PAGE_SET_CURRENT_PAGE_NUMBER'),
    setQueryValue: actionCreator<string>('ADMIN_ENTRIES_PAGE_SET_QUERY_VALUE'),
    setDeletingID: actionCreator<string | null>('ADMIN_ENTRIES_PAGE_SET_ENTRY_ID'),
}


export const AdminEntryEditPageActions = {
    setEntryID: actionCreator<string>('ADMIN_ENTRY_EDIT_PAGE_SET_ENTRY_ID'),
    setName: actionCreator<string>('ENTRY_EDIT_PAGE_SET_NAME'),
    setEmail: actionCreator<string>('ENTRY_EDIT_PAGE_SET_EMAIL'),
    setAge: actionCreator<number>('ENTRY_EDIT_PAGE_SET_AGE'),
    setDesiredOccupation: actionCreator<string>('ENTRY_EDIT_PAGE_SET_DESIRED_OCCUPATION'),
    setReasonForApply: actionCreator<string>('ENTRY_EDIT_PAGE_SET_REASON_FOR_APPLY'),
    setDeletingID: actionCreator<string | null>('ENTRY_EDIT_PAGE_SET_ENTRY_ID'),
}

export const AdminRecruitEditPageActions = {
    setRecruitID: actionCreator<string | undefined>('ADMIN_RECRUIT_EDIT_PAGE_SET_RECRUIT_ID'),
    setPosition: actionCreator<string>('ADMIN_RECRUIT_EDIT_PAGE_SET_POSITION'),
    setContent: actionCreator<string>('ADMIN_RECRUIT_EDIT_PAGE_SET_CONTENT'),
    setQualification: actionCreator<string>('ADMIN_RECRUIT_EDIT_PAGE_SET_QUALIFICATION'),
    setEmploymentStatus: actionCreator<string>('ADMIN_RECRUIT_EDIT_PAGE_SET_EMPLOYMENT_STATUS'),
    setDeletingID: actionCreator<string | null>('ADMIN_RECRUIT_EDIT_PAGE_SET_ENTRY_ID'),
}

export const AdminLoginPageActions = {
    setUserName: actionCreator<string>('ADMIN_LOGIN_PAGE_SET_USER_NAME'),
    setPassWord: actionCreator<string>('ADMIN_LOGIN_PAGE_SET_PASSWORD'),
}

export const AdminResetPasswordPageActions = {
    setEmail: actionCreator<string>('ADMIN_RESET_PASSWORD_PAGE_SET_EMAIL'),
}