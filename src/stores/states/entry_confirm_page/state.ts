export interface EntryConfirmPageState {
    name: string;
    email: string;
    age: number;
    desiredOccupation: string;
    reasonForApply: string;
}