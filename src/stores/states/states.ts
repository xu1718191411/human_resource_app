import {EntryPageState} from './entry_page/state';
import {EntryEntity, RecruitEntity} from "./common_states";
import {AdminEntriesPageStates, AdminEntryEditPageStates, AdminRecruitEditPageStates} from "./admin/admin_page_states";
import {AdminRecruitsPageStates} from "./admin/admin_recruits_page_states";
import {AdminLoginPageStates} from "./admin/admin_login_page_states";
import {EntryConfirmPageState} from "./entry_complete_page/state";
import {AdminResetPasswordPageStates} from "./admin/admin_reset_password_page_states";

export interface States {
    inputValue: string,
    selectedValue: string,
    entryEntities: EntryEntity[],
    applyEntryEntity: EntryEntity | null,
    recruits: RecruitEntity[],
}

export interface PageStates {
    entryPageState: EntryPageState,
    entryConfirmPage: EntryConfirmPageState,
    adminEntriesPageStates: AdminEntriesPageStates,
    adminEntryEditPageStates: AdminEntryEditPageStates,
    adminRecruitEditPageStates: AdminRecruitEditPageStates,
    adminRecruitsPageStates: AdminRecruitsPageStates,
    adminLoginPageStates: AdminLoginPageStates,
    adminResetPasswordStates: AdminResetPasswordPageStates,
}

export interface GlobalStates {
    states: States,
    pageStates: PageStates,
}