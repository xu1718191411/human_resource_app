import {ItemsNumber} from "../../../constants/constants";

export interface AdminEntriesPageStates {
    title: string;
    detailDisplay: boolean;
    itemsNumber: ItemsNumber;
    currentPageNumber: number;
    queryValue: string;
    deletingID: string | null;
}

export interface AdminEntryEditPageStates {
    entryID: string;
    editNameVal: string;
    editEmailVal: string;
    editAgeVal: number;
    editDesiredOccupationVal: string;
    editApplyReasonVal: string;
    editStatusVal: string;
    deletingID: string | null;
}

export interface AdminRecruitEditPageStates {
    recruitID: string | undefined;
    position: string;
    content: string;
    qualification: string;
    employmentStatus: string;
    deletingID: string | null;
}