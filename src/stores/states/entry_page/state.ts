export interface EntryPageState {
    name: string;
    email: string;
    age: number;
    desiredOccupation: string;
    reasonForApply: string;
    formError: string | null;
}