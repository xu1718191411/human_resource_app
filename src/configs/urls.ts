export const URL_INDEX = "/";
export const URL_RECRUIT = "/recruits";
export const URL_ENTRY = "/entry";
export const URL_ENTRY_CONFIRM = "/entry-confirm/:entry_id";
export const GET_URL_ENTRY_CONFIRM = (entryID:string) => `/entry-confirm/${entryID}`;
export const URL_ENTRY_COMPLETE = "/entry-complete";


export const URL_ADMIN_LOGIN = "/admin/login";
export const URL_ADMIN_LOGOUT = "/admin/logout";
export const URL_ADMIN_ENTRIES = "/admin/entries"
export const URL_ADMIN_ENTRY_DETAIL = "/admin/entries/:entry_id";
export const GET_URL_ADMIN_ENTRY_DETAIL = (entryID:string) => `/admin/entries/${entryID}`;

export const URL_ADMIN_RECRUITS = "/admin/recruits";
export const URL_ADMIN_RECRUIT_DETAIL = "/admin/recruit-edit/:recruit_id";
export const GET_URL_ADMIN_RECRUIT_DETAIL = (recruitID:string) => `/admin/recruit-edit/${recruitID}`;

export const URL_ADMIN_RECRUIT_CREATE = "/admin/recruit-create";


export const URL_RESET_PASSWORD = "/admin/reset-password";
export const URL_RESET_PASSWORD_COMPLETE = "/admin/reset-password-complete/:email";
export const GET_URL_RESET_PASSWORD_COMPLETE = (email:string) => `/admin/reset-password-complete/${email}`


export const URL_CONTACT = "/contact";
export const URL_NEWS = "/news";
export const URL_ABOUT = "/about";
export const URL_HOME = "/home";