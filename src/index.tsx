import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import reportWebVitals from './reportWebVitals';
import {Router, Route} from "react-router-dom";
import {EntryPage} from './pages/entry_page';
import {EntryConfirmPage} from './pages/entry_confirm_page';
import {EntryCompletePage} from "./pages/entry_complete_page";

import {AdminLoginPage} from "./pages/admin/admin_login_page";
import {AdminEntriesPage} from "./pages/admin/admin_entries_page";
import {AdminResetPasswordPage} from "./pages/admin/admin_reset_password_page";

import './styles/index.scss';
import {URLS} from './configs'
import {AdminResetPasswordCompletePage} from './pages/admin/admin_reset_password_complete_page';
import {RecruitsPage} from './pages/recruits_page';
import {AdminRecruitEditPage} from './pages/admin/admin_recruit_edit_page';
import {AdminRecruitsPage} from './pages/admin/admin_recruits_page';
import {AdminEntryEditPage} from './pages/admin/admin_entry_edit_page';
import {AdminLogOutPage} from './pages/admin/admin_logout_page';
import {Provider} from "react-redux";
import {createReduxStore} from "./stores/store";
import {createBrowserHistory} from "history";
import Modal from "react-modal";
Modal.setAppElement('#root')
const createAPP = () => {

    const history = createBrowserHistory()

    const firebaseConfig = {
        apiKey: "AIzaSyAl5zOKWmUKm8nI_dqNapDcwSTyb9yPGRE",
        authDomain: "human-resource-app-3941c.firebaseapp.com",
        projectId: "human-resource-app-3941c",
        storageBucket: "human-resource-app-3941c.appspot.com",
        messagingSenderId: "245102138157",
        appId: "1:245102138157:web:aa91a057e92a55e6aaf9a4",
        measurementId: "G-ELRMHR76EE"
    };

    const store = createReduxStore(history, firebaseConfig);
    ReactDOM.render(
        <Provider store={store}>
            <Router history={history}>
                <Route exact path={URLS.URL_INDEX} component={RecruitsPage}></Route>
                <Route exact path={URLS.URL_ENTRY} component={EntryPage}></Route>
                <Route exact path={URLS.URL_ENTRY_CONFIRM} component={EntryConfirmPage}></Route>
                <Route exact path={URLS.URL_ENTRY_COMPLETE} component={EntryCompletePage}></Route>
                <Route exact path={URLS.URL_RECRUIT} component={RecruitsPage}></Route>

                <Route exact path={URLS.URL_ADMIN_LOGIN} component={AdminLoginPage}></Route>
                <Route exact path={URLS.URL_ADMIN_LOGOUT} component={AdminLogOutPage}></Route>

                <Route exact path={URLS.URL_ADMIN_ENTRIES} component={AdminEntriesPage}></Route>
                <Route exact path={URLS.URL_ADMIN_ENTRY_DETAIL} component={AdminEntryEditPage}></Route>

                <Route exact path={URLS.URL_ADMIN_RECRUITS} component={AdminRecruitsPage}></Route>
                <Route exact path={URLS.URL_ADMIN_RECRUIT_DETAIL} component={AdminRecruitEditPage}></Route>
                <Route exact path={URLS.URL_ADMIN_RECRUIT_CREATE} component={AdminRecruitEditPage}></Route>

                <Route exact path={URLS.URL_RESET_PASSWORD} component={AdminResetPasswordPage}></Route>
                <Route exact path={URLS.URL_RESET_PASSWORD_COMPLETE} component={AdminResetPasswordCompletePage}></Route>
            </Router>
        </Provider>,
        document.getElementById('root')
    );

}

// If you want to start measuring performance in your app, pass a function
// to log results (for example: reportWebVitals(console.log))
// or send to an analytics endpoint. Learn more: https://bit.ly/CRA-vitals
reportWebVitals();


const main = async () => {
    createAPP()
}

main()