import {EntryEntity, RecruitEntity} from "../stores/states/common_states";
import * as firebase from "firebase";

export const parseEntry = (response: firebase.firestore.DocumentData | undefined,id:string): EntryEntity => {
    if (response === undefined) {
        return {
            id: '',
            name: '',
            email: '',
            age: 0,
            desiredOccupation: '',
            reasonForApply: '',
        }
    }

    return {
        id: id,
        name: response.name,
        email: response.email,
        age: response.age,
        desiredOccupation: response.desired_occupation,
        reasonForApply: response.reason_for_apply,
    }
}

export const parseEntries = (response: firebase.firestore.QuerySnapshot): EntryEntity[] => {
    let result: EntryEntity[] = [];
    response.forEach((doc) => {
        const item = parseEntry(doc.data(),doc.id)
        result.push(item);
    });
    return result;
}


export const parseRecruits = (response: firebase.firestore.QuerySnapshot): RecruitEntity[] => {
    let result: RecruitEntity[] = [];
    response.forEach((doc) => {
        const item = parseRecruit(doc.data(),doc.id)
        result.push(item);
    })
    return result;
}

export const parseRecruit = (response: firebase.firestore.DocumentData | undefined, id:string): RecruitEntity => {
    if (response === undefined) {
        return {
            id: '',
            position: '',
            content: '',
            qualification: '',
            employment_status: '',
        }
    }

    return {
        id: id,
        position: response.position,
        content: response.content,
        qualification: response.qualification,
        employment_status: response.employment_status,
    }
}