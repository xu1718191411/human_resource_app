import {range} from "../utils/utils";

export const ageRange: number[] = range(18,80);

interface PaginationItem {
    displayName:string;
    value:ItemsNumber;
}

export type ItemsNumber = 5 | 10 | 15 | 20 | 25;

export const PaginationItems: PaginationItem[] = [
    {
        displayName:"5項目",
        value:5,
    },
    {
        displayName:"10項目",
        value:10,
    },
    {
        displayName:"15項目",
        value:15,
    },
    {
        displayName:"20項目",
        value:20,
    },
    {
        displayName:"25項目",
        value:25,
    },
]

export const customStyles = {
    content : {
        top                   : '30%',
        left                  : '50%',
        right                 : 'auto',
        bottom                : 'auto',
        marginRight           : '-50%',
        transform             : 'translate(-50%, -50%)'
    }
};