import * as firebase from "firebase";

export interface FirebaseConfig {
    apiKey: string
    authDomain: string
    projectId: string
    storageBucket: string
    messagingSenderId: string
    appId: string
    measurementId: string
}

export interface postNewEntryParam {
    age: number,
    desired_occupation: string,
    email: string,
    name: string,
    reason_for_apply: string,
}

export interface postNewRecruitParam {
    position:string,
    content:string,
    qualification:string,
    employment_status:string,
}

export interface EntryCloudFunctionPostData {
    name:string;
    email:string;
    age:number;
    desired_occupation:string;
    apply_for_reason:string;
}

export class FirebaseService {
    firebaseClient: firebase.app.App;
    public firebaseAuthClient:FirebaseAuthClient;

    constructor(firebaseConfig: FirebaseConfig) {
        this.firebaseClient = firebase.initializeApp(firebaseConfig);
        this.firebaseAuthClient = new FirebaseAuthClient(this.firebaseClient);
    }

    public async postNewEntry(param: postNewEntryParam):Promise<string> {
        const db = this.firebaseClient.firestore().collection("entries");
        const res = await db.add(param);
        return res.id;
    }

    public getAllEntries = async (): Promise<firebase.firestore.QuerySnapshot> => {
        const db = this.firebaseClient.firestore().collection("entries");
        const res = await db.get();
        return res
    }

    public getEntry = async (entryID: string): Promise<firebase.firestore.DocumentSnapshot> => {
        const db = this.firebaseClient.firestore().collection("entries");
        const res = await db.doc(entryID).get();
        return res;
    }

    public queryEntries = async (query: string): Promise<firebase.firestore.QuerySnapshot[]> => {
        const db = this.firebaseClient.firestore().collection("entries");
        const result:firebase.firestore.QuerySnapshot[] = []
        const snapshotsByName = await db.orderBy("name").startAt(query).endAt(query + '\uf8ff').get();

        result.push(snapshotsByName);
        const snapshotsByEmail = await db.orderBy("email").startAt(query).endAt(query + '\uf8ff').get();

        result.push(snapshotsByEmail);
        const snapshotsByID = await db.where(firebase.firestore.FieldPath.documentId(), '==', query).get()

        result.push(snapshotsByID);
        return result;
    }

    public updateEntry = async (entryID: string, name: string, email: string, age: number, desireOccupation: string, applyForReason: string) => {
        const db = this.firebaseClient.firestore().collection("entries");
        const res = await db.doc(entryID);
        await res.update({
            name: name,
            age: age,
            email: email,
            desired_occupation: desireOccupation,
            reason_for_apply: applyForReason,
        })
    }

    public deleteEntry = async (entryID: string) => {
        const db = this.firebaseClient.firestore().collection("entries");
        const res = await db.doc(entryID);
        await res.delete();
    }

    public postRecruit = async (param:postNewRecruitParam):Promise<firebase.firestore.DocumentReference> => {
        const db = this.firebaseClient.firestore().collection("recruits");
        const res = await db.add(param)
        return res
    }

    public getRecruits = async (): Promise<firebase.firestore.QuerySnapshot> => {
        const db = this.firebaseClient.firestore().collection("recruits");
        const res = await db.get();
        return res
    }

    public getRecruit = async (recruitID: string): Promise<firebase.firestore.DocumentSnapshot> => {
        const db = this.firebaseClient.firestore().collection("recruits");
        const res = await db.doc(recruitID).get();
        return res;
    }

    public updateRecruit = async (recruitID: string,position:string,content:string,qualification:string,employmentStatus:string) => {
        const db = this.firebaseClient.firestore().collection("recruits");
        const res = await db.doc(recruitID);
        await res.update({
            position: position,
            content: content,
            qualification: qualification,
            employment_status: employmentStatus,
        })
    }

    public deleteRecruit = async (recruitID: string) => {
        const db = this.firebaseClient.firestore().collection("recruits");
        const res = await db.doc(recruitID);
        await res.delete();
    }
}

export class FirebaseAuthClient{
    firebaseClient: firebase.app.App;

    constructor(firebaseClient: firebase.app.App) {
        this.firebaseClient = firebaseClient;
    }

    public loginWithMailAndPassword = async (email:string, password:string):Promise<firebase.auth.UserCredential> => {
        const res = await this.firebaseClient.auth().signInWithEmailAndPassword(email,password);
        return res;
    }

    public signOut = async ()=> {
        await this.firebaseClient.auth().signOut();
    }

    public checkISLogin = async ():Promise<boolean> => {
        return new Promise<boolean>(((resolve, reject) => {
            this.firebaseClient.auth().onAuthStateChanged(function(user) {
                if (user) {
                    resolve(true);
                } else {
                    resolve(false);
                }
            });
        }))
    }

    public resetPassword = async (value:string) => {
       const res = await this.firebaseClient.auth().sendPasswordResetEmail(value);
       return res;
    }
}