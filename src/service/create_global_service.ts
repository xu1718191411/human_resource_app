import {GlobalService} from "./global_service";
import {FirebaseConfig, FirebaseService} from "./firebase_service";

export const createGlobalService = (firebaseConfig:FirebaseConfig, history: any):GlobalService => {
    const firebaseService = new FirebaseService(firebaseConfig);
    return new GlobalService(firebaseService,history);
}