import {FirebaseService} from "./firebase_service";
import {History} from "history";

export class GlobalService {
    public firebaseService: FirebaseService;
    public history: History;

    constructor(service: FirebaseService, history:  History<any>) {
        this.firebaseService = service;
        this.history = history;
    }
}